set terminal svg size 750,750 enhanced font "Helvetica, 30"
set output "gene_family_heatmap.svg"


#
# Various ways to create a 2D heat map from ascii data
#

#set title "Gene Families"
unset key
set tic scale 0

# Color runs from white to green
set palette rgbformula -7,2,-7
set palette maxcolors 4
set cbrange [0:4]
set cblabel "Number of paralogs"
set cbtics 0,1,3 scale 2 offset -2, 1.5

set xrange [-0.5:15.5]
set yrange [-0.5:3.5]
set ylabel "Gene Family id"
set xtics rotate by 90 offset 0, char -0.4 ("obr" 0, "osaindica" 1, "osa" 2, "ped" 3, "bdi" 4, "hvu" 5, "ttu" 6, "tae" 7, "oth" 8, "zjn" 9, "cam" 10, "sit" 11, "zma-ph207" 12, "zma" 13, "sbi" 14, "ssp" 15)
set ytics rotate by -45  ("gene family 1" 0, "gene familiy 2" 1, "..." 2, "gene family n" 3)

$map1 << EOD
1 1 1 2 1 1 1 1 1 1 1 1 1 1 2 1
1 1 2 1 1 1 1 1 1 2 2 3 2 1 2 1
0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
1 1 1 1 1 1 2 1 3 1 1 1 1 2 1 1
EOD

set view map
splot '$map1' matrix with image pixels
#, $map1 matrix using 1:2:($3 == 0 ? "" : sprintf("%g",$3) ) with labels
