
set terminal svg size 1500,750 enhanced font "Helvetica, 30"
set output "motifa_plot.svg"


set xrange [ 0.07 : 0.95 ] noreverse writeback
set xlabel "Branch Length Score"
set yrange [ 1 : * ] noreverse writeback
set ylabel "Number of gene families"
set y2label "Confidence score"
set y2tics
set y2range [ 0 : 1.0 ] noreverse writeback
set title "Motif a"
set tics nomirror
set style fill transparent solid 0.5 noborder

set key ins vert cent right

set arrow from 0.65,14400 to 0.95,14400 nohead front lc 3 lw 2  dashtype "-"
set arrow from 0.65,14400 to 0.65,0 nohead front lc 1 lw 2  dashtype "-"
set label "0.9" at 0.95,14400 center offset char 2,char 0  tc rgb "skyblue"
set label "0.65" at 0.65,0 left offset char -1,char -1.5  tc rgb "dark-violet"
set label "minimum BLS for 0.9 confidence for motif a" at 0.65,0 left offset char -1,char -2.5  tc rgb "dark-violet"


plot "good_motif_counts.txt" using 1:2 with filledcurves y1=0 title "" lc 1, \
     "good_motif_counts.txt" using 1:2 with linespoints title "Gene family counts" lc 1 pt 2, \
     "median_counts.txt" using 1:2 with filledcurves y1=0 title "" lc 2, \
     "median_counts.txt" using 1:2 with linespoints title "Median gene family counts" lc 2 pt 2, \
     "good_motif_conf.txt" using 1:2 with linespoints axes x1y2 title "confidence" lc 3 pt 2
