package org.apache.spark

import org.apache.spark.rdd.HadoopPartition
import scala.reflect.ClassTag
import java.util.StringTokenizer
import scala.collection.mutable.ArrayBuffer
import scala.collection.JavaConverters._
import org.apache.spark.util.Utils
import java.io.{File, FilenameFilter, IOException, PrintWriter, OutputStreamWriter, BufferedWriter,DataOutputStream}
import scala.io.{Codec, Source}
import java.util.concurrent.atomic.AtomicReference
import org.apache.spark.rdd.RDD
import scala.io.Codec.string2codec
import org.apache.log4j.{Level, Logger}
import scala.collection.mutable.ListBuffer
import scala.collection.Map // ensure same maps...
import java.util.UUID
import org.apache.hadoop.fs.Path
import scala.collection.JavaConversions._

class StringPipedRDD[T: ClassTag](
    prev: RDD[T],
    command: Seq[String],
    procName: String,
    tmpFolder:String = "/tmp/",
    var logLevel: Level = null,
    envVars: Map[String, String] = Map(),
    separateWorkingDir: Boolean = false,
    bufferSize: Int = 8192,
    encoding: String = Codec.defaultCharsetCodec.name)
  extends RDD[String](prev) with be.ugent.intec.ddecap.Logging {

  class NotEqualsFileNameFilter(filterName: String) extends FilenameFilter {
    def accept(dir: File, name: String): Boolean = {
      !name.equals(filterName)
    }
  }

  override def getPartitions: Array[Partition] = firstParent[T].partitions

  override def compute(split: Partition, context: TaskContext): Iterator[String] = {
    logLevel = Logger.getLogger(getClass()).getLevel()
    val time = System.nanoTime

    // val tmp_file = tmpFolder + "%d_%s".format(split.index, UUID.randomUUID()) + ".txt";
    // val my_command = command.map(x => x.replace("INPUT", tmp_file))
    // // print the data to a file and use as input
    // var fsize = 0
    // new PrintWriter(tmp_file) {
    //   for (elem <- firstParent[String].iterator(split, context)) {
    //     fsize += elem.size
    //     write(elem);
    //   }
    //  close
    // }
    // info("["+ split.index+ "] has written " + fsize + " to tmp file")


    val my_command = command //.map(x => x.replace("INPUT", tmp_file))
    val pb = new ProcessBuilder(my_command.asJava)
    val cmd = my_command.mkString(" ")
    info("["+ split.index+ "] running " + cmd)

    // Add the environmental variables to the process.
    // set hadoop_home
    val currentEnvVars = pb.environment()
    envVars.foreach { case (variable, value) => currentEnvVars.put(variable, value) }
    // for compatibility with Hadoop which sets these env variables
    // so the user code can access the input filename
    if (split.isInstanceOf[HadoopPartition]) {
      currentEnvVars.putAll(split.asInstanceOf[HadoopPartition].getPipeEnvVars().asJava)
    }
    // set HADOOP_HOME/CLASSPATH for motifiterator
    val hadoop_home = if(sys.env.get("HADOOP_HOME").isEmpty)
                          currentEnvVars.get("HADOOP_CONF_DIR").substring(0, currentEnvVars.get("HADOOP_CONF_DIR").lastIndexOf("/etc/hadoop"))
                      else sys.env.get("HADOOP_HOME").get
    currentEnvVars.putAll(Map("HADOOP_HOME" -> hadoop_home, "CLASSPATH" -> System.getProperty("java.class.path")).asJava)

    // When spark.worker.separated.working.directory option is turned on, each
    // task will be run in separate directory. This should be resolve file
    // access conflict issue
    val taskDirectory = "tasks" + File.separator + java.util.UUID.randomUUID.toString
    var workInTaskDirectory = false
    debug("["+ split.index+ "] taskDirectory = " + taskDirectory)
    if (separateWorkingDir) {
      val currentDir = new File(".")
      debug("currentDir = " + currentDir.getAbsolutePath())
      val taskDirFile = new File(taskDirectory)
      taskDirFile.mkdirs()

      try {
        val tasksDirFilter = new NotEqualsFileNameFilter("tasks")

        // Need to add symlinks to jars, files, and directories.  On Yarn we could have
        // directories and other files not known to the SparkContext that were added via the
        // Hadoop distributed cache.  We also don't want to symlink to the /tasks directories we
        // are creating here.
        for (file <- currentDir.list(tasksDirFilter)) {
          val fileWithDir = new File(currentDir, file)
          Utils.symlink(new File(fileWithDir.getAbsolutePath()),
            new File(taskDirectory + File.separator + fileWithDir.getName()))
        }
        pb.directory(taskDirFile)
        workInTaskDirectory = true
      } catch {
        case e: Exception => {logError("Unable to setup task working directory: " + e.getMessage +
          " (" + taskDirectory + ")", e); throw e}
      }
    }


    val proc = pb.start()
    val env = SparkEnv.get
    val childThreadException = new AtomicReference[Throwable](null)

    // Start a thread to print the process's stderr to ours
    new Thread(s"stderr reader for $procName") {
      override def run(): Unit = {
        val err = proc.getErrorStream
        try {
          for (line <- Source.fromInputStream(err)(Codec.defaultCharsetCodec.name).getLines) {
            info("["+ split.index+ "] " + line)
          }
        } catch {
          case t: Throwable => childThreadException.set(t)
        } finally {
          err.close()
        }
      }
    }.start()

    // Start a thread to feed the process input from our parent's iterator
    new Thread(s"stdin writer for $procName") {
      override def run(): Unit = {
        TaskContext.setTaskContext(context)
        var fsize = 0
        val out = new PrintWriter(new BufferedWriter(
          new OutputStreamWriter(proc.getOutputStream, encoding), bufferSize))
        try {
          for (elem <- firstParent[String].iterator(split, context)) {
            fsize += elem.size
            out.println(elem)
          }
        } catch {
          case t: Throwable => childThreadException.set(t)
        } finally {
          info("["+ split.index+ "] has written " + fsize + " to stdin")
          out.close()
        }
      }
    }.start()

    var lines =  new ListBuffer[String]()



    new Thread(s"stdout reader for $procName") {
      override def run(): Unit = {
        val out = proc.getInputStream()
        try {
          for (line <- Source.fromInputStream(out)(Codec.defaultCharsetCodec.name).getLines) {
            lines += line
          }
        } catch {
          case t: Throwable => childThreadException.set(t)
        } finally {
          out.close()
        }
      }
    }.start()
    val exitStatus = proc.waitFor()

    //
    // val f = new File(tmp_file)
    // if(f.exists())
    //     f.delete()
    //

    // cleanup task working directory if used
    if (workInTaskDirectory) {
      scala.util.control.Exception.ignoring(classOf[IOException]) {
        Utils.deleteRecursively(new File(taskDirectory))
      }
      logDebug(s"Removed task working directory $taskDirectory")
    }
    if (exitStatus != 0) {
      error(s"Subprocess exited with status $exitStatus. " +
        s"Command ran: " + my_command.mkString(" "))
      throw new IllegalStateException(s"Subprocess exited with status $exitStatus. " +
        s"Command ran: " + my_command.mkString(" "))
    }

    val t = childThreadException.get()
    if (t != null) {
      proc.destroy()
      throw t
    }

    info("["+ split.index+ "] finished " + procName + " in " + (System.nanoTime-time)/1.0e9 + "s")

    // return
    lines.iterator


  }
}
