package be.ugent.intec.ddecap

import java.nio.file.{Files, Paths}

import scala.collection.immutable.HashSet
import be.ugent.intec.ddecap.dna.BinaryDnaStringFunctions._
import be.ugent.intec.ddecap.dna.LongEncodedDna._
import be.ugent.intec.ddecap.dna.BlsVector
import be.ugent.intec.ddecap.rdd.RDDFunctions._
import be.ugent.intec.ddecap.spark.BlsParquet._
import be.ugent.intec.ddecap.tools.Tools
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.{Dataset, DataFrame, SparkSession}
import org.apache.spark.storage.StorageLevel
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.conf.Configuration
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.rdd.RDD
import java.net.URI
// import be.ugent.intec.ddecap.dna.BlsVector

object BlsSpeller extends Logging {

  case class Config(
      mode: String = "getMotifs",
      parquetInput: Boolean = false,
      input: String = "",
      motifs: String = "",
      fasta: String = "",
      partitions: Int = 8,
      alignmentBased: Boolean = false,
      lowmemory : Boolean = false,
      plots : Boolean = false,
      bindir: String = "",
      output: String = "",
      maxDegen: Int = 4,
      minMotifLen: Int = 8,
      maxMotifLen: Int = 9,
      minBlsScore: Double = 0.0,
      geneStartInclusive: Boolean = true, // todo implement this
      geneStopInclusive: Boolean = false, // and this
      geneStrandAware: Boolean = false, // promotor region is reverse complemented if on negative strand -> reverses gene start stop inclusiveness
      alphabet: Int = 2, // 0: exact, 1: exact+N, 2: exact+2fold+M, 3: All
      familyCountCutOff: Int = 0,
      similarityScore: Int = -1,
      useRC: Boolean = true,
      positionOutput: Int = 0,
      filterBasedOnGenesMin: Int = Int.MinValue,
      filterBasedOnGenesMax: Int = Int.MaxValue,
      motiflist: String = "",
      confidenceType: Int = 0,
      oneOutput: Boolean = false,
      mergeParquet: Boolean = false,
      interactive: Boolean = false,
      getPermutation: Boolean = false,
      keepOneBasedCoordinates: Boolean = false,
      emitRandomLowConfidenceScoreMotifs: Int = 0,
      numPartitionsForSecondStepFactor : Int = 8,
      minPartitionsForSecondStep: Int = 4,
      backgroundModelCount: Int = -1,
      confidenceScoreCutOff: Double = 0.5,
      thresholdList: List[Float] = List(0.15f, 0.5f, 0.6f, 0.7f, 0.9f, 0.95f),
      persistLevel: StorageLevel = StorageLevel.DISK_ONLY
    )

  var sc: SparkContext = null

  def main(args: Array[String]) {

    val optionConfig = parseOptionsAndStart(args)
    optionConfig match {
      case Some(config) =>
        Logger.getLogger("org").setLevel(Level.ERROR)
        Logger.getLogger("akka").setLevel(Level.ERROR)
        var conf: SparkConf = null;
        conf = new SparkConf()
              .set("spark.serializer", "org.apache.spark.serializer.KryoSerializer")
              .set("spark.kryo.registrator","be.ugent.intec.ddecap.spark.BlsKryoSerializer")
              .set("spark.kryoserializer.buffer.mb","128")
              .set("spark.kryo.registrationRequired", "false")
              .set("spark.executor.processTreeMetrics.enabled", "true")
              .set("spark.sql.shuffle.partitions", Math.max(60, config.partitions).toString);
        val spark = SparkSession.builder
                                .appName("BLS Speller")
                                .config(conf)
                                .getOrCreate()
        sc = spark.sparkContext

        info("Apache Spark verion is " + sc.version)
        info("serializer is " + sc.getConf.get("spark.serializer", "org.apache.spark.serializer.JavaSerializer"))

        runPipeline(spark, config)
        sc.stop()
        spark.stop()
      case None => throw new Exception("arguments missing")
    }
  }

  def parseOptionsAndStart(args: Array[String]) : Option[Config] = {
    val parser = new scopt.OptionParser[Config]("bls-speller") {
      val p = getClass.getPackage
      val name = p.getImplementationTitle
      val version = p.getImplementationVersion
      head(name, version) // adjust version here

      // global options
      opt[String]('i', "input").action( (x, c) =>
        c.copy(input = x) ).text("Location of the input files/folders.").required()
      opt[String]('o', "output").action( (x, c) =>
        c.copy(output = x)).text("The output directory (spark output).").required()
      opt[String]("persist_level").action( (x, c) =>
          x match {
            case "mem_disk" => c.copy(persistLevel = StorageLevel.MEMORY_AND_DISK)
            case "mem_disk_ser" => c.copy(persistLevel = StorageLevel.MEMORY_AND_DISK_SER)
            case "disk" => c.copy(persistLevel = StorageLevel.DISK_ONLY)
            case _ => c.copy(persistLevel = StorageLevel.MEMORY_AND_DISK)
          }
         ).text("Sets the persist level for RDD's: mem_disk, mem_disk_ser [default], disk")
      opt[Unit]("skipRC").action( (_, c) =>
        c.copy(useRC = false) ).text("Skip the Reverse Complement in finding and matching motifs.")
      opt[String]("bls_thresholds").action( (x, c) =>
      {
        val list = x.split(",").map(x => x.toFloat).toList
        c.copy(thresholdList = list)
      }).text("List of BLS threshold sepparated by a comma (example: 0.15,0.5,0.6,0.7,0.9,0.95).").required()


      // command to find motifs in ortho groups
        cmd("getMotifs").action( (_, c) => c.copy(mode = "getMotifs") ).
          text("get motifs of interest.").
          children(
            opt[Int]("alphabet").action( (x, c) =>
              c.copy(alphabet = x) ).text("Sets the alphabet used in motif iterator: 0: Exact, 1: Exact+N, 2: Exact+2fold+M, 3: All. Default is 2."),
      	    opt[Unit]("parquet").action( (_, c) =>
      	      c.copy(parquetInput = true) ).text("Folder(s) of Parquet motif files."),
      	    opt[Unit]("merge_parquet").action( (_, c) =>
      	      c.copy(mergeParquet = true) ).text("Parquet input folder contains data from multiple motifIterator runs."),
      	    opt[Int]("confidence_type").action( (x, c) =>
      	      c.copy( confidenceType = if (x == 1 || x == 0) x else 0) ).text("Confidence type in cutoff, 0: confidence score, 1: negative binomial distribution."),
            opt[Unit]("lowmemory").action( (_, c) =>
              c.copy(lowmemory = true) ).text("use low memory mode.").hidden,
            opt[Unit]("plots").action( (_, c) =>
              c.copy(plots = true) ).text("show plots of distributions.").hidden,
            opt[Int]("min_len").action( (x, c) =>
              c.copy(minMotifLen = x) ).text("Sets the minimum length of a motif.").required(),
            opt[Int]("max_len").action( (x, c) =>
              c.copy(maxMotifLen = x) ).text("Sets the maximum length of a motif, this is not inclusive (i.e. length < maxLength).").required(),
            opt[Int]("bg_model_count").action( (x, c) =>
              c.copy(backgroundModelCount = x) ).text("Sets the count of motifs in the background model. Default is -1: count is based on the number of permutations of the group."),
            opt[Int]("similarity_score").action( (x, c) =>
              c.copy(similarityScore = x) ).text("Uses a similarity score to find the most dissimilar motifs in the background model. 0: Binary diff, 1: Hamming distance, 2: Levenshtein distance, Default is -1 [disabled]."),
            opt[Int]("emitrandommotifs").action( (x, c) =>
              c.copy(emitRandomLowConfidenceScoreMotifs = x) ).text("Emit random motifs below the c score threshold, one in every [int] motifs. Should be a high number, 100 000 or more. "),
            opt[String]("motiflist").action( (x, c) =>
              c.copy(motiflist = x) ).text("Input list of motifs to output, skips the rest. "),

            opt[String]('b', "bindir").action( (x, c) =>
              c.copy(bindir = x) ).text("Location of the directory containing all binaries.").required(),

            opt[Int]('p', "partitions").action( (x, c) =>
              c.copy(partitions = x) ).text("Number of partitions used by executors.").required(),

            opt[Int]("degen").action( (x, c) =>
              c.copy(maxDegen = x) ).text("Sets the max number of degenerate characters.").required(),

            opt[Unit]("AB").action( (_, c) =>
              c.copy(alignmentBased = true) ).text("Alignment based motif discovery.")
          )
        cmd("mergeParquet").action( (_, c) => c.copy(mode = "mergeParquet") ).
          text("merge parquet files into a single parquet file.") //.children()

        cmd("locateMotifs").action( (_, c) => c.copy(mode = "locateMotifs") ).
          text("locates the found motifs in the given Ortho Groups.").
          children(
            opt[Unit]("parquet").action( (_, c) =>
              c.copy(parquetInput = true) ).text("Folder(s) of Parquet motif files."),

            opt[String]('m', "motifs").action( (x, c) =>
              c.copy(motifs = x) ).text("Folder with the motifs found by the first step of BLS Speller.").required(),

            opt[Unit]("get_permutation").action( (_, c) =>
              c.copy(getPermutation = true) ).text("Also emit a permutation of every valid motif as validation."),

            opt[String]("fasta").action( (x, c) =>
              c.copy(fasta = x) ).text("Provide a fasta file or folder with genes and location in the genome. The output will now only be locations in this genome, with the motifs."),

          // currently not implemented in the c++ score! for merged counts...
            opt[Int]("max_len").action( (x, c) =>
              c.copy(maxMotifLen = x) ).text("Sets the maximum length of a motif, this is not inclusive (i.e. length < maxLength).").required(),

            opt[Double]("min_bls").action( (x, c) =>
              c.copy(minBlsScore = x) ).text("Min bls score, emit motif locations only with a bls score of at least this value. Default is 0, i.e. no filtering. "),

            opt[Unit]("AB").action( (_, c) =>
              c.copy(alignmentBased = true) ).text("Alignment based motif discovery."),

            opt[String]('b', "bindir").action( (x, c) =>
              c.copy(bindir = x) ).text("Location of the directory containing all binaries.").required(),

            opt[Unit]('I', "interactive").action( (_, c) =>
              c.copy(interactive = true) ).text("Interactively search for good family count and confidence score cutoffs."),

            opt[Int]('p', "partitions").action( (x, c) =>
              c.copy(partitions = x) ).text("Number of partitions used by executors.").required(),
            opt[String]("motiflist").action( (x, c) =>
              c.copy(motiflist = x) ).text("Input list of motifs to also check. "),
            opt[Int]("degen").action( (x, c) =>
              c.copy(maxDegen = x) ).text("Sets the max number of degenerate characters.").required(),

            opt[Unit]("gene_start_exclusive").action( (_, c) =>
              c.copy(geneStartInclusive = false) ).text("Gene promotor region start position is exclusive [default is inclusive]"),

            opt[Unit]("keep_one_based_coordinates").action( (_, c) =>
              c.copy(keepOneBasedCoordinates = true) ).text("Keeps one based coordinates given in fasta file (else converts to 0 based)"),

            opt[Unit]("gene_stop_inclusive").action( (_, c) =>
              c.copy(geneStopInclusive = true) ).text("Gene promotor region stop position is inclusive [default is exclusive]"),

            opt[Unit]("gene_strand_aware").action( (_, c) =>
              c.copy(geneStrandAware = true) ).text("Gene promotor region is strand aware, i.e. gene on negative strand start, stop position exclusiveness is reversed. [default false]"),

            opt[Unit]("gene_pos").action( (_, c) =>
              c.copy(positionOutput = 2) ).text("Print only the motif position in gene promotor instead of chromosome [default is chr position with added column that shows gene pos]."),

            opt[Unit]("chr_pos").action( (_, c) =>
              c.copy(positionOutput = 1) ).text("Print only the chromosome position instead of in gene promotor [default is chr position with added column that shows gene pos]."),

            opt[Unit]("one_output").action( (_, c) =>
              c.copy(oneOutput = true) ).text("Do not split output in bed files per motif. hdfs dfs -getmerge is needed afterwards."),

            opt[String]("filter_based_on_nr_genes").action( (x, c) =>
              c.copy(filterBasedOnGenesMin = x.split(",")(0).toInt, filterBasedOnGenesMax = x.split(",")(1).toInt) ).text("Filter motifs based on the number of genes they appear in, min and max required separated by a comma.").hidden(), // hidden cause not yet tested

            opt[Int]("fam_cutoff").action( (x, c) =>
              c.copy(familyCountCutOff = Math.max(0, x - 1)) ).text("Sets the number of families a motif needs to be part of to be valid. Default is 1."),

            opt[Double]("conf_cutoff").action( (x, c) =>
              c.copy(confidenceScoreCutOff = x) ).text("Sets the cutoff for confidence scores. Default is 0.5.")
          )
    }
    parser.parse(args, Config())
  }
  def parseMemory(memory: String) : Int = { // returns memory string to memory in megabytes
    val tmp = memory.toLowerCase()
    if(tmp.contains("g")) {
      return tmp.dropRight(1).toInt * 1024
    } else if (tmp.contains("m")) {
      return tmp.dropRight(1).toInt
    } else if (tmp.contains("k")) {
      return tmp.dropRight(1).toInt / 1024
    } else { // in megabytes of not specified
      return tmp.toInt
      // throw new Exception("Invalid memory string: " + memory)
    }
  }

  def toBinary(i: Int, digits: Int = 8) = "0000000" + i.toBinaryString takeRight digits

  def runPipeline(spark: SparkSession, config: Config) : Unit = {
    import spark.implicits._
    Timer.startTime()
    var tools = new Tools(config.bindir);
    val conf_ = new Configuration(sc.hadoopConfiguration)
    val outfs = FileSystem.get(new URI(config.output), conf_);
    val outPath = new Path(config.output);
    if(outfs.exists(outPath)) outfs.delete(outPath, true);
    // info("setting # partitions to " + config.partitions)
    var families = tools.readOrthologousFamilies(config.input, config.partitions, sc);
    // families.persist(StorageLevel.DISK_ONLY)
    // val familiescount : Long = families.count
    // info("family count: " + familiescount);

    // determine executor stats
    val sparkConf = sc.getConf;
    val cpus = sparkConf.get("spark.task.cpus", "1").toInt
    val memory = parseMemory(sparkConf.get("spark.executor.memory"))
    val overheadMem = parseMemory(sparkConf.get("spark.executor.memoryOverhead", Math.max(384, memory * 0.1).toInt.toString + "M"))
    info("Executors [" + memory + "MB + " + overheadMem + "MB OH / " + cpus + " CPUS]")
    val totalMemory =  overheadMem + memory

    if(config.mode == "getMotifs") {
      info("getting motifs from '" + config.input + "'")
      val secondStepPartitions = Math.max(config.minPartitionsForSecondStep, config.numPartitionsForSecondStepFactor* config.partitions)
      var inputmotiflist : HashSet[String] = HashSet.empty[String];
      if (config.motiflist != "") {
        inputmotiflist = HashSet() ++ sc.textFile(config.motiflist).collect
      }

      val parquetFiles= if(!config.parquetInput) {
        info("running motifIterator [length" + (if(config.minMotifLen == config.maxMotifLen - 1) " " + config.minMotifLen  else ("s " + config.minMotifLen + " until " + config.maxMotifLen))  + " with " + config.maxDegen + " degenerate characters (" +
          (if (config.alphabet == 0) "exact" else if (config.alphabet == 1) "N" else if (config.alphabet == 2) "twofold & N" else "full Iupac") + ")]")
        System.getProperty("user.dir")
        val defaultfs = sc.hadoopConfiguration.get("fs.defaultFS")
        var outputuri = if(config.input.startsWith("/")) config.output else System.getProperty("user.dir") + "/" + config.output
        outputuri = defaultfs + outputuri
        tools.iterateMotifsPerOrthoGroup(families, conf_, config.mode, config.alignmentBased, config.alphabet, config.maxDegen,
                                    config.minMotifLen, config.maxMotifLen, config.thresholdList, config.useRC, cpus, outputuri);
      } else config.input

      // read and process parquet
      val motifsdf = if (config.mergeParquet || !config.parquetInput) countPerMotif(spark.read.parquet(parquetFiles)) else spark.read.parquet(parquetFiles)
      val motifs = dataframeToRDD(motifsdf)
      // assumes motifs are unique here! (after merge)
      val motifsWithBlsCounts = countAndCollectUniqueMotifs(motifs, Math.max(motifs.partitions.length, config.partitions));
      val plotsOutPath = new Path(if (config.output.last == '/' ) config.output.dropRight(1) + ".plots" else config.output  + ".plots")
      if(config.plots) {
        info("creating folder " + plotsOutPath.toString())
        if(outfs.exists(plotsOutPath)) outfs.delete(plotsOutPath, true);
        outfs.mkdirs(plotsOutPath);
      }
      val output : RDD[(ImmutableDna, BlsVector, List[Float], ImmutableDna)] =
        processVectorGroups(motifsWithBlsCounts, config.thresholdList, config.backgroundModelCount, config.similarityScore, config.familyCountCutOff,
            config.confidenceScoreCutOff, inputmotiflist, config.confidenceType, config.emitRandomLowConfidenceScoreMotifs,
            if(config.plots) plotsOutPath.toString() else "", conf_)
      output.map(x => (LongToDnaString(x._1, getDnaLength(x._4)) + "\t" + x._2 + "\t" + x._3.mkString("\t") + "\t")).saveAsTextFile(config.output);
      info(Timer.measureTotalTime("BlsSpeller"))
    } else if (config.mode == "mergeParquet") {
      val motifsdf=countPerMotif(spark.read.parquet(config.input))
      motifsdf.write.parquet(config.output)
      info(Timer.measureTotalTime("BlsSpeller - merge Parquet Motifs"))
    } else if(config.mode == "locateMotifs") {
      info("locating motifs from '" + config.input + "'")
      var familyCountCutOff = config.familyCountCutOff
      var confidenceScoreCutOff = config.confidenceScoreCutOff
      var allmotifsdf: DataFrame = spark.emptyDataFrame;
      var allmotifsrdd : RDD[String] = sc.emptyRDD;
      if(config.parquetInput)
        allmotifsdf = spark.read.parquet(config.motifs);
      else
        allmotifsrdd =sc.textFile(config.motifs, config.partitions);
      var motifsetdf: Dataset[String] = spark.emptyDataset[String];
      var motifsetrdd : RDD[String] = sc.emptyRDD;
      if(config.parquetInput)
        motifsetdf = filterMotifs(spark, allmotifsdf, config.thresholdList, familyCountCutOff, confidenceScoreCutOff)
      else
        motifsetrdd = tools.filterMotifs(allmotifsrdd, config.thresholdList, familyCountCutOff, confidenceScoreCutOff);

      if(config.interactive) {
        info((if(config.parquetInput) motifsetdf.count else motifsetrdd.count) + " motifs with family count cutoff at " + familyCountCutOff + " and confidence cutoff at " + confidenceScoreCutOff)
        info("do you want to change the cutoffs? y/N: ")
        var a=scala.io.StdIn.readLine()
        while(a.toLowerCase().equals("y") || a.toLowerCase().equals("yes")) {
          info("give new family count cutoff [current is " + familyCountCutOff + "]: ")
          a = scala.io.StdIn.readLine()
          if(!a.isEmpty) {
            familyCountCutOff=a.toInt
          }
          info("give new confidence cutoff [current is " + confidenceScoreCutOff + "]: ")
          a = scala.io.StdIn.readLine()
          if(!a.isEmpty) {
            confidenceScoreCutOff= a.toDouble
          }

          if(config.parquetInput)
            motifsetdf = filterMotifs(spark, allmotifsdf, config.thresholdList, familyCountCutOff, confidenceScoreCutOff)
          else
            motifsetrdd = tools.filterMotifs(allmotifsrdd, config.thresholdList, familyCountCutOff, confidenceScoreCutOff);
          info((if(config.parquetInput) motifsetdf.count else motifsetrdd.count) + " motifs with family count cutoff at " + familyCountCutOff + " and confidence cutoff at " + confidenceScoreCutOff)
          info("do you want to change the cutoffs? y/N: ")
          a=scala.io.StdIn.readLine()
        }
      }

      // create permutations of each motif if needed
      var motifs : Array[String] = if (config.getPermutation && !config.parquetInput) tools.addPermutationPerMotif(config.motifs, config.partitions, sc, config.thresholdList, motifsetrdd, config.output) else (if(config.parquetInput) motifsetdf.collect else motifsetrdd.collect).sorted
      var custommotifs : Array[String] = Array.empty[String]
      if (config.motiflist != "") {
        custommotifs = sc.textFile(config.motiflist).map(x => x + "\t0").collect
        motifs = (motifs ++ custommotifs).sorted
      }
      if(motifs.length == 0) {
        throw new Exception("0 motifs left after filtering on confidence score of " + config.confidenceScoreCutOff + ".")
      }
      // motifs.foreach(println)
      val broadcastMotifs = sc.broadcast(motifs)
      var extramotifsource = ""
      if(config.getPermutation) {
        extramotifsource = " (with permutations"
        if(!custommotifs.isEmpty){
          extramotifsource += " and " + custommotifs.length + " custom motifs)"
        } else {
          extramotifsource += ")"
        }
      } else if(!custommotifs.isEmpty){
        extramotifsource = " (with " + custommotifs.length + " custom motifs)"
      }
      info("motif count after filter" + extramotifsource + ": " + broadcastMotifs.value.size);
      val motifLocations = tools.locateMotifs(families, config.mode, config.alphabet, broadcastMotifs, config.alignmentBased, config.maxDegen, config.maxMotifLen, config.thresholdList, config.minBlsScore, config.useRC);

      if(config.fasta == "") {
        motifLocations.sortBy(_._1).map(x => x._1.geneid + '\t' + x._1.posingene + '\t' + x._2).saveAsTextFile(config.output);
      } else {
        info("positionOutput: " + (if(config.positionOutput == 0) "chromosome position + promotor position" else if(config.positionOutput == 1) "chromosome position" else "promotor position"));
        info("useRC: " + config.useRC);
        info("geneStrandAware: " + config.geneStrandAware);

        val fastafs = FileSystem.get(new URI(config.output), conf_);
        // check if fasta is a *fasta/fa file or a directory
        motifLocations.persist(config.persistLevel)
        val fastafiles = if("\\.fasta$|\\.fa$".r.findFirstIn(config.fasta) != None) {
          info("matching with " + config.fasta)
          // // read the fasta file and get position in genome -> RDD[(gene_id, start_pos, end_pos)]
          // val (genomeLocations, bcVar) = tools.joinFastaAndLocations(config.fasta, config.partitions, sc, motifLocations, config.useRC, config.positionOutput, config.geneStartInclusive, config.geneStopInclusive, config.geneStrandAware)
          // // tools.parseGenomeLocations(genomeLocations, config.positionOutput).saveAsTextFile(config.output);
          // val bedlines = tools.parseGenomeLocationsPerMotif(genomeLocations, config.positionOutput)
          // tools.saveBedFilePerMotif(bedlines, config.output, conf_ , motifs.size / 2 + 1);
          // bcVar.destroy()
           Array(new Path(config.fasta));

        } else {
          fastafs.listStatus(new Path(config.fasta)).filter(x => !x.isDirectory && "fasta|fa$".r.findFirstIn(x.toString) != None).map(_.getPath)

          //
          // for(fasta<-fastafiles) {
          //   info("matching with " + fasta.getName)
          //   val (genomeLocations, bcVar) = tools.joinFastaAndLocations(fasta.toString, config.partitions, sc, motifLocations, config.useRC, config.positionOutput, config.geneStartInclusive, config.geneStopInclusive, config.geneStrandAware)
          //
          //   val bedlines = tools.parseGenomeLocationsPerMotif(genomeLocations, config.positionOutput)
          //   tools.saveBedFilePerMotif(bedlines, config.output + "/" + fasta.getName, conf_ , motifs.size / 2 + 1);
          //   bcVar.destroy()
          //   // tools.parseGenomeLocations(genomeLocations, config.positionOutput).saveAsTextFile(config.output + "/" + fasta.getName);
          // }
        }
        if (config.oneOutput) info("not splitting bed file per motif")
        tools.parseGenomeLocationsPerFasta(fastafiles, config.output, config.partitions, sc, motifLocations, config.useRC, config.positionOutput,
                            config.geneStartInclusive, config.geneStopInclusive, config.geneStrandAware, config.keepOneBasedCoordinates, conf_ , motifs.size / 2 + 1, config.oneOutput)
        motifLocations.unpersist()
      }
      info(Timer.measureTotalTime("BlsSpeller - locate Motifs"))
    } else {
      throw new Exception("invalid command")
    }
  }
}
