package be.ugent.intec.ddecap.tools

import be.ugent.intec.ddecap.Logging
import org.apache.commons.math3.util.CombinatoricsUtils._
import org.apache.commons.math3.special.Gamma._
import org.apache.commons.math3.special.Beta._
import org.apache.log4j.{Level, Logger}

// all formulas are from wikipedia....
object Distribution {
  val logger = Logger.getLogger("be.ugent.intec.ddecap.tools.Distribution");
  def modelDistribution(mean:Double, variance:Double, zeroInflated: Double) : AbstractDistribution = { // model distribution based on mean and variance
    if(variance == 0.0) // means only a single value which will be the mean, which can be 0!
      return new ZeroInflatedSingleValueDistribution(mean, zeroInflated);
    else if (zeroInflated > 0.0)
      return if(variance > mean)
        modelZeroInflatedNegativeBinomial(mean, variance, zeroInflated)
      else if (variance < mean)
        modelZeroInflatedBinomial(mean, variance, zeroInflated)
      else
        modelZeroInflatedPoisson(mean, zeroInflated)
    else
      return if(variance > mean)
        modelNegativeBinomial(mean, variance)
      else if (variance < mean)
        modelBinomial(mean, variance)
      else
        modelPoisson(mean)
  }
  def modelZeroInflatedNegativeBinomial(mean: Double, variance: Double, pi: Double) : ZeroInflatedNegativeBinomial = {
    // zero inflated negative binomial
    val r: Double = Math.max(Math.pow(mean, 2) / (variance - mean), 1)
    val p : Double = mean / (mean + r) // check if 0 <= p <= 1???
    return new ZeroInflatedNegativeBinomial(p, r, pi)
  }
  def modelNegativeBinomial(mean: Double, variance: Double) : NegativeBinomial = {
    // negative binomial
    val r: Double = Math.max(Math.pow(mean, 2) / (variance - mean), 1)
    val p : Double = mean / (mean + r) // check if 0 <= p <= 1???
    // logger.info("negative binomial distribution with p/r: " + p + "/" + r + " vs old " + ptmp + "/" + rtmp)
    // r is not equal to n!!! r => We observe this sequence until a predefined number r of successes have occurred
    return new NegativeBinomial(p, r)
  }
  def modelZeroInflatedBinomial(mean: Double, variance: Double, pi: Double) : ZeroInflatedBinomial = {
    // zero inflated negative binomial
    val n : Double = Math.max(mean / (1 - variance / mean), 1)
    val p : Double = mean / n // check if 0 <= p <= 1???
    return new ZeroInflatedBinomial(p, n, pi)
  }
  def modelBinomial(mean: Double, variance: Double) : Binomial = {
    // binomial
    val n : Double = Math.max(mean / (1 - variance / mean), 1)
    val p : Double = mean / n // check if 0 <= p <= 1???
    // logger.info("negative binomial distribution with p/n: " + p + "/" + n )
    return new Binomial(p, n) // supports  0 > n
  }
  def modelZeroInflatedPoisson(mean: Double, pi: Double) : ZeroInflatedPoisson = {
    // zero inflated negative poisson
    val lambda : Double = mean
    return new ZeroInflatedPoisson(lambda, pi)
  }
  def modelPoisson(mean: Double) : Poisson = {
    // poisson
    val lambda : Double = mean
    // logger.info("poisson distribution with lambda " + lambda)
    return new Poisson(lambda)
  }
}

abstract class AbstractDistribution() extends Serializable with Logging {
  // assumes x is always >= 0!
  def probability(x: Int) : Double // pmf
  def logProbability(x: Int) : Double // log of pmf
  def cumulativeProbability(x: Int) : Double // cdf
  override def toString() : String
  def negLog10InvCumulativeProbability(x: Int): Double = {
      return -Math.log(1-cumulativeProbability(x))/Math.log(10)
  }
}
class NegativeBinomial(p: Double, r: Double) extends AbstractDistribution {
  val q : Double = 1 - p
  def probability(x: Int) : Double = {
    // info("binom coef of " + (x + r - 1) + " of " + x)
    return binomialCoefficientDouble((x + r - 1).toInt, x)*Math.pow(q, r) * Math.pow(p, x)
  }
  def logProbability(x: Int) : Double = {
    // info("log binom coef of " + (x + r - 1) + " of " + x)
    return binomialCoefficientLog((x + r - 1).toInt, x) + r * Math.log(q) + x * Math.log(p)
  }
  def cumulativeProbability(x: Int) : Double = {
    // I_p(r, k + 1)
    // return regularizedBeta(p, r, x + 1)
    // F_binomial(k;n = k + r ; p = p)
    return regularizedBeta(q, r, x + 1)
  }
  override def toString() = "NegativeBinomial"
}
class Binomial(p: Double, n: Double) extends AbstractDistribution {
  val q : Double = 1 - p
  def probability(x: Int) : Double = {
    return binomialCoefficientDouble(n.toInt, x)*Math.pow(p, x) * Math.pow(q, n - x)
  }
  def logProbability(x: Int) : Double = {
    return Math.log(binomialCoefficientLog(n.toInt, x)) + x * Math.log(p) + (n - x) * Math.log(q)
  }
  def cumulativeProbability(x: Int) : Double = {
    // I_q(n - k, 1 + k)
    return if (x < n) regularizedBeta(q, n - x, x + 1) else 1.0
  }
  override def toString() = "Binomial"
}
class Poisson(lambda: Double) extends AbstractDistribution {
  def probability(x: Int) : Double = {
    return Math.pow(lambda, x)*Math.exp(-lambda)/factorial(x)
  }
  def logProbability(x: Int) : Double = {
    return x*Math.log(lambda) + (-lambda)*Math.log(Math.E) - Math.log(factorial(x))
  }
  def cumulativeProbability(x: Int) : Double = {
    // Math.floor(x+1) not needed as we are using ints, so using x + 1 instead
    return regularizedGammaQ(x + 1, lambda)
  }
  override def toString() = "Poisson"
}
class ZeroInflatedSingleValueDistribution(value:Double, pi: Double) extends AbstractDistribution {
  def probability(x: Int) : Double = {
    return if (x == 0) pi else if (x == value) (1 - pi) else 0
  }
  def logProbability(x: Int) : Double = {
    return if (x == 0) Math.log(pi) else if (x == value) Math.log(1 - pi) else Double.NaN
  }
  def cumulativeProbability(x: Int) : Double = {
    // I_p(r, k + 1)
    return if (x == 0 || x < value || (x == value && pi == 0)) pi else 1.0; // (x == value && pi == 0) to avoid emiiting motifs like AAAAAAAAA, CCCCCCCCC... etc
  }
  override def toString() = "ZeroInflatedSingleValueDistribution"
}
class ZeroInflatedNegativeBinomial(p: Double, r: Double, pi: Double) extends AbstractDistribution { // pi is fraction of zeros, while r and p are calculated based on means/variances without 0's
  val q : Double = 1 - p
  val nbin = new NegativeBinomial(p, r)
  def probability(x: Int) : Double = {
    // info("binom coef of " + (x + r - 1) + " of " + x)
    return if (x == 0) pi else (1-pi)*nbin.probability(x)
  }
  def logProbability(x: Int) : Double = {
    throw new NotImplementedError()
  }
  def cumulativeProbability(x: Int) : Double = {
    // I_p(r, k + 1)
    return if (x == 0) pi else pi + (1-pi)*nbin.cumulativeProbability(x)
  }
  override def toString() = "ZeroInflatedNegativeBinomial"
}
class ZeroInflatedBinomial(p: Double, r: Double, pi: Double) extends AbstractDistribution { // pi is fraction of zeros, while r and p are calculated based on means/variances without 0's
  val q : Double = 1 - p
  val bin = new Binomial(p, r)
  def probability(x: Int) : Double = {
    // info("binom coef of " + (x + r - 1) + " of " + x)
    return if (x == 0) pi else (1-pi)*bin.probability(x)
  }
  def logProbability(x: Int) : Double = {
    throw new NotImplementedError()
  }
  def cumulativeProbability(x: Int) : Double = {
    // I_p(r, k + 1)
    return if (x == 0) pi else pi + (1-pi)*bin.cumulativeProbability(x)
  }
  override def toString() = "ZeroInflatedBinomial"
}
class ZeroInflatedPoisson(lambda: Double, pi: Double) extends AbstractDistribution { // pi is fraction of zeros, while r and p are calculated based on means/variances without 0's
  val poisson = new Poisson(lambda)
  def probability(x: Int) : Double = {
    // info("binom coef of " + (x + r - 1) + " of " + x)
    return if (x == 0) pi else (1-pi)*poisson.probability(x)
  }
  def logProbability(x: Int) : Double = {
    throw new NotImplementedError()
  }
  def cumulativeProbability(x: Int) : Double = {
    // I_p(r, k + 1)
    return if (x == 0) pi else pi + (1-pi)*poisson.cumulativeProbability(x)
  }
  override def toString() = "ZeroInflatedPoisson"
}
