package be.ugent.intec.ddecap.tools


import org.apache.spark.rdd.RDD
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.SparkContext
import org.apache.hadoop.io.{LongWritable, Text}
import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.storage.StorageLevel
import be.ugent.intec.ddecap.Logging
import be.ugent.intec.ddecap.spark.ConfigSerDeser
import be.ugent.intec.ddecap.tools.FileUtils.deleteRecursivelyAndCreate
import java.util.StringTokenizer
import java.io._
import java.net.URI
import org.apache.log4j.Level
import org.apache.log4j.Logger
import org.apache.spark.BinaryPipedRDD
import org.apache.spark.HashPartitioner
import java.nio.ByteBuffer
import be.ugent.intec.ddecap.dna.BlsVectorFunctions._
import be.ugent.intec.ddecap.dna.{ImmutableDnaPair, ImmutableDnaWithBlsVectorByte, BlsVector, ImmutableDnaWithBlsVector, MotifKey, MotifInfo, GeneInfo, GenomePosition, PositionData, MotifBedInfo}

@SerialVersionUID(227L)
class Tools(val bindir: String) extends Serializable with Logging {
  type ImmutableDna = Long
  val DegenerationTable = Map(
    'A' -> 1,
    'B' -> 3,
    'C' -> 1,
    'D' -> 3,
    'G' -> 1,
    'H' -> 3,
    'M' -> 2,
    'K' -> 2,
    'N' -> 4,
    'R' -> 2,
    'S' -> 2,
    'T' -> 1,
    'V' -> 3,
    'W' -> 2,
    'Y' -> 2
  )
  val ComplementTable = Map(
    'A' -> 'T',
    'B' -> 'V',
    'C' -> 'G',
    'D' -> 'H',
    'G' -> 'C',
    'H' -> 'D',
    'M' -> 'K',
    'K' -> 'M',
    'N' -> 'N',
    'R' -> 'Y',
    'S' -> 'S',
    'T' -> 'A',
    'V' -> 'B',
    'W' -> 'W',
    'Y' -> 'R'
  )
  def getDegenerationMultiplier(in: String) : Int = {
    // info("motif: " + in)
    var ret = 1
    for (i <- in.size -1 to 0 by -1) {
      ret *= DegenerationTable(in(i))
    }
    ret
  }

  def reverseComplement(in: String) : String = {
    var ret = ""
    for (i <- in.size -1 to 0 by -1) {
      ret += ComplementTable(in(i))
    }
    ret
  }


  private def tokenize(command: String): Seq[String] = {
    val buf = new ArrayBuffer[String]
    val tok = new StringTokenizer(command)
    while(tok.hasMoreElements) {
      buf += tok.nextToken()
    }
    buf
  }

  private def toBinaryFormat(rdd: RDD[String]) : RDD[Array[Byte]] = {
    rdd.map(x => x.getBytes)
  }

  def readOrthologousFamilies(input: String, partitions: Int, sc: SparkContext): RDD[String] = {
    val tmp = sc.wholeTextFiles(input, partitions).flatMap(x => {
      val tmp = x._2.split("\n");
      val list: ListBuffer[String] = new ListBuffer[String]();
      // split per ortho family
      var i = 0;
      while (i < tmp.size) {
        var ortho = "";
        while(tmp(i).isEmpty()) {i+=1;}
        // var name = tmp(i);
        ortho += tmp(i) + "\n"; // name
        i+=1;
        ortho += tmp(i) + "\n"; // newick
        i+=1;
        val N = tmp(i).toInt;
        ortho += tmp(i) + "\n"; // Count
        i+=1;
        for(j <- 0 until N) {
          // if(i >= tmp.length) {
          //   info("problem in array: " + tmp.mkString("\n"))
          //   throw new Exception("problem with " + name + " on gene ids line " + j + " in array with length " + tmp.length)
          // }
          ortho += tmp(i) + "\n"; // ortho name
          i+=1;
          // if(i >= tmp.length) {
            // info("problem in array: " + tmp.mkString("\n"))
            // throw new Exception("problem with " + name + " on sequences line " + j + " in array with length " + tmp.length)
          // }
          ortho += tmp(i) + "\n"; // DNA string
          i+=1;
        }
        list.append(ortho);
      }
      list
    })
    // tmp
    tmp.repartition(partitions); //  too many partitions for whole file -> repartition based on size???! of the familiy (# characters)
  }


  def parseGeneLocationString(line: String) : (String, String, Boolean, Int, Int) = {
    val tmp = line.split(":")
    if(tmp.length == 6) { // >Zm00001d027230::1:43289:44289:+
      return (tmp(0).substring(1), tmp(2), (tmp(5) == "+"), tmp(3).toInt, tmp(4).toInt)
    } else if (tmp.length == 4) { // >Zm00001d027230::1:39350-48995
      val lastdash = tmp(3).lastIndexOf("-") // accounts for negative values
      val start = tmp(3).substring(0, lastdash).toInt
      val end = tmp(3).substring(lastdash+1).toInt
      return (tmp(0).substring(1), tmp(2), true, start, end)
    } else if (tmp.length == 7) { // >Zm00001d027230:1:39289-51837:+:39289-44289:44947-45665:49837-51837 // position can be negative??
      val lastdash = tmp(2).lastIndexOf("-")
      val start = tmp(2).substring(0, lastdash).toInt
      val end = tmp(2).substring(lastdash+1).toInt
      return (tmp(0).substring(1), tmp(1), (tmp(3) == "+"), start, end)
    } else
      throw new Exception("invalid gene information in id line in fasta file: " + line)
  }

  def getGeneSizeMap(fasta: String, partitions: Int, sc: SparkContext): RDD[(String, (String, String, Int))] = {
    val genesToKeep = sc.textFile(fasta, partitions).map(x => (if(x.startsWith(">")) x else x.length.toString))
                        .zipWithIndex()
                        .map(x => (x._2/2, List(x._1)))
                        .reduceByKey(_++_)
                        .map(x => {
                          val (geneId, chromosome, isFwStrand, genestart, genestop) = parseGeneLocationString(x._2.filter(_.startsWith(">"))(0));
                          val genelength = x._2.filter(! _.startsWith(">"))(0).toInt;
                          (geneId, (chromosome, if(isFwStrand) "+" else "-", genelength))
                        } )
    genesToKeep
  }
  def getGeneLocationMap(fasta: String, partitions: Int, sc: SparkContext, geneStartInclusive: Boolean, geneStopInclusive: Boolean, geneStrandAware: Boolean): RDD[(String, GeneInfo)] = {
    val genesToKeep = sc.textFile(fasta, partitions).filter(x => x.startsWith(">")).map(x => {
          val (geneId, chromosome, isFwStrand, genestart, genestop) = parseGeneLocationString(x)
          var start : Int = 0
          var stop : Int = 0
          if (isFwStrand || !geneStrandAware) { // forward strand, regular positions or gene is on reverse strand but not strand aware so exclusiveness is not swapped
            start = genestart + (if (geneStartInclusive) 0 else 1)
            stop = genestop + (if (geneStopInclusive) 1 else 0)
          } else { // gene is on reverse strand (isFwStrand == false) and strand aware ( geneStrandAware == true), so exclusiveness is swapped
            start = genestart + (if (geneStopInclusive) 0 else 1)
            stop = genestop + (if (geneStartInclusive) 1 else 0)
          }
          (geneId, GeneInfo("", chromosome, isFwStrand, start, stop))
        })
    genesToKeep
  }
def getExtendedGeneLocationMap(fasta: String, partitions: Int, sc: SparkContext, geneStartInclusive: Boolean, geneStopInclusive: Boolean, geneStrandAware: Boolean): RDD[(String, GeneInfo)] = {
  val genesToKeep = sc.textFile(fasta, partitions).filter(x => x.startsWith(">")).map(x => {
        val (geneId, chromosome, isFwStrand, genestart, genestop) = parseGeneLocationString(x)
        var start : Int = 0
        var stop : Int = 0
        if (isFwStrand || !geneStrandAware) { // forward strand, regular positions or gene is on reverse strand but not strand aware so exclusiveness is not swapped
          start = genestart + (if (geneStartInclusive) 0 else 1)
          stop = genestop + (if (geneStopInclusive) 1 else 0)
        } else { // gene is on reverse strand (isFwStrand == false) and strand aware ( geneStrandAware == true), so exclusiveness is swapped
          start = genestart + (if (geneStopInclusive) 0 else 1)
          stop = genestop + (if (geneStartInclusive) 1 else 0)
        }
        (geneId, GeneInfo(fasta.substring(fasta.lastIndexOf('/') + 1), chromosome, isFwStrand, start, stop))
      })
  genesToKeep
}

  def flatMapMotifLocations(motifLocations: RDD[(MotifKey, MotifInfo)], broadcastGeneMap: Broadcast[scala.collection.immutable.Map[String, GeneInfo]],
      useRC: Boolean, geneStrandAware: Boolean, keepOneBasedCoordinates : Boolean = false) : RDD[(GenomePosition, MotifBedInfo)] = {
      motifLocations.flatMap(x => {
          if(broadcastGeneMap.value.contains(x._1.geneid)) { // x = (geneid, posingene) (motifrep, blsscore, strand)
            val geneinfo = broadcastGeneMap.value(x._1.geneid);
            val positioninfo = GenomePosition(geneinfo.fasta, geneinfo.chromosome,  // chr
             // most left position of motif
             if(x._2.strand) { // motif pos strand
               if(!geneStrandAware || geneinfo.isFwStrand) { // gene on pos strand or is not strand aware
                 PositionData(geneinfo.start + x._1.posingene + (if(keepOneBasedCoordinates) 0 else -1), x._1.posingene)                                                                                // motif FW strand FW
               } else { // gene on neg strand
                 PositionData(geneinfo.stop - x._1.posingene - x._2.motifrep.length - 1 + (if(keepOneBasedCoordinates) 0 else -1), x._1.posingene)                                                      // motif FW strand RV
               }
             }
             else if(useRC) { // motif negative strand
               if(!geneStrandAware || geneinfo.isFwStrand) { // gene on pos strand or is not strand aware
                 PositionData(geneinfo.stop + x._1.posingene - x._2.motifrep.length + (if(keepOneBasedCoordinates) 0 else -1), geneinfo.stop - geneinfo.start + x._1.posingene - x._2.motifrep.length)  // motif RV strand FW
               } else { // gene on neg strand
                 PositionData(geneinfo.start - x._1.posingene - 1 + (if(keepOneBasedCoordinates) 0 else -1), geneinfo.stop - geneinfo.start + x._1.posingene - x._2.motifrep.length)                    // motif RV strand RV
               }
             }
             else throw new Exception("this cannot happen") // should already be caught
          )
          val bedinfo = MotifBedInfo(if(geneinfo.isFwStrand) "+" else "-", // strand
            x._2.motifrep, // motif
            x._2.blsscore, // bls score
            x._1.geneid, // geneid
            x._2.strand // motif strand
            )
          Some((positioninfo, bedinfo))
        } else None
      })
  }

  def joinFastaAndLocations(fasta: String, partitions: Int, sc: SparkContext, motifLocations: RDD[(MotifKey, MotifInfo)],
        useRC: Boolean, geneStartInclusive: Boolean, geneStopInclusive: Boolean, geneStrandAware: Boolean, keepOneBasedCoordinates : Boolean): (RDD[(GenomePosition, MotifBedInfo)], Broadcast[scala.collection.immutable.Map[String, GeneInfo]]) = {

    val geneMap =  getGeneLocationMap(fasta, partitions, sc, geneStartInclusive, geneStopInclusive, geneStrandAware) // map((geneId => (chromosome, isFwStrand, start, stop)))
    val broadcastGeneMap = sc.broadcast(geneMap.collect.toMap) // map(geneid => (chrom. fwstrand, seqlength))

    (flatMapMotifLocations(motifLocations, broadcastGeneMap, useRC, geneStrandAware, keepOneBasedCoordinates), broadcastGeneMap)
  }

    def parseGenomeLocationsPerFasta(fastafiles: Array[Path], outputDir: String, partitions: Int, sc: SparkContext, motifLocations: RDD[(MotifKey, MotifInfo)],
          useRC: Boolean, positionOutput: Int, geneStartInclusive: Boolean, geneStopInclusive: Boolean, geneStrandAware: Boolean, keepOneBasedCoordinates : Boolean, conf: Configuration,
          approxNumberOfKeys: Int = -1, oneOutput: Boolean = false) = {

      var fullGeneMap =  getExtendedGeneLocationMap(fastafiles(0).toString, partitions, sc, geneStartInclusive, geneStopInclusive, geneStrandAware) // map((geneId => (chromosome, isFwStrand, start, stop)))
      for (i <- 1 until fastafiles.length) {
        fullGeneMap = fullGeneMap.union(getExtendedGeneLocationMap(fastafiles(i).toString, partitions, sc, geneStartInclusive, geneStopInclusive, geneStrandAware) )
      }
      val broadcastGeneMap = sc.broadcast(fullGeneMap.collect.toMap) // map(geneid => (chrom. fwstrand, seqlength))

      // match motifiterator output to fasta info
      val genomeLocations = flatMapMotifLocations(motifLocations, broadcastGeneMap, useRC, geneStrandAware, keepOneBasedCoordinates)

      // map to bed files
      val bedInfoRDD = if(positionOutput == 0)
          genomeLocations.map(x => ((x._1.fasta, x._2.motif),
            x._1.chromosome + '\t' + x._1.position.posingenome + '\t' + (x._1.position.posingenome + x._2.motif.length) + '\t' + x._2.gene_strand +
            '\t' + x._2.geneid + '\t' + x._2.motif + '\t' + x._2.blsscore +
            '\t' + x._1.position.posingene + ',' + (x._1.position.posingene + x._2.motif.length))
          )
        else
          genomeLocations.map(x => ((x._1.fasta, x._2.motif),
            x._1.chromosome + '\t' + x._1.position.posingenome + '\t' + (x._1.position.posingenome + x._2.motif.length) + '\t' + x._2.gene_strand +
            '\t' + x._2.geneid + '\t' + x._2.motif + '\t' + x._2.blsscore)
          )

        // write output
      val outDir = if(outputDir.last == '/') outputDir else outputDir + "/";
      if (!oneOutput) {
        val parts = if(approxNumberOfKeys > 0) approxNumberOfKeys else bedInfoRDD.partitions.size
        val serdeserconf = new ConfigSerDeser(conf)
        val outfs = FileSystem.get(new URI(outDir), serdeserconf.get());
        val fs = FileSystem.get(new URI(outDir), serdeserconf.get());
        val outPath = new Path(outDir);
        if(fs.exists(outPath)) fs.delete(outPath, true);
        fs.mkdirs(outPath);
        for (i <- 0 until fastafiles.length) { // create all fasta dirs
          fs.mkdirs(new Path(outDir + fastafiles(i).getName + "/"));
        }

        bedInfoRDD.partitionBy(new HashPartitioner(parts)).mapPartitions(it => {
          val fs = FileSystem.get(new URI(outDir), serdeserconf.get());
          val fileWriterMap: scala.collection.mutable.Map[(String, String), OutputStream] = scala.collection.mutable.Map.empty[(String, String), OutputStream];
          while(it.hasNext ) {
            val t = it.next();
            fileWriterMap.getOrElseUpdate(t._1, fs.create(new Path(outDir + t._1._1 + "/" + t._1._2 + ".bed"))).write((t._2 + "\n").getBytes);
          }
          fileWriterMap.foreach(x => x._2.close());
          fileWriterMap.map(_._1).iterator // motif names
        }).count // makes sure its written to data

        broadcastGeneMap.destroy()
        fs.createNewFile(new Path(outDir + "_SUCCESS"));
      } else {

        bedInfoRDD.map(_._2).saveAsTextFile(outDir)

        broadcastGeneMap.destroy()
      }
  }


  def parseGenomeLocations(genomeLocations: RDD[((GenomePosition, MotifBedInfo))], positionOutput: Int) : RDD[String] = {
      if(positionOutput == 0)
        genomeLocations.sortBy(x=> (x._2.motif, x._1.position.posingenome)).map(x =>
            x._1.chromosome + '\t' + x._1.position.posingenome + '\t' + (x._1.position.posingenome + x._2.motif.length) + '\t' + x._2.gene_strand +
            '\t' + x._2.geneid + '\t' + x._2.motif + '\t' + x._2.blsscore +
            '\t' + x._1.position.posingene + ',' + (x._1.position.posingene + x._2.motif.length)
          )
      else
        genomeLocations.sortBy(x=> (x._2.motif, x._1.position.posingenome)).map(x =>
          x._1.chromosome + '\t' + x._1.position.posingenome + '\t' + (x._1.position.posingenome + x._2.motif.length) + '\t' + x._2.gene_strand +
          '\t' + x._2.geneid + '\t' + x._2.motif + '\t' + x._2.blsscore
        )
  }
  def parseGenomeLocationsPerMotif(genomeLocations: RDD[((GenomePosition, MotifBedInfo))], positionOutput: Int) : RDD[(String, String)] = {
      if(positionOutput == 0)
        genomeLocations.map(x => (x._2.motif,
          x._1.chromosome + '\t' + x._1.position.posingenome + '\t' + (x._1.position.posingenome + x._2.motif.length) + '\t' + x._2.gene_strand +
          '\t' + x._2.geneid + '\t' + x._2.motif + '\t' + x._2.blsscore +
          '\t' + x._1.position.posingene + ',' + (x._1.position.posingene + x._2.motif.length))
        )
      else
        genomeLocations.map(x => (x._2.motif,
          x._1.chromosome + '\t' + x._1.position.posingenome + '\t' + (x._1.position.posingenome + x._2.motif.length) + '\t' + x._2.gene_strand +
          '\t' + x._2.geneid + '\t' + x._2.motif + '\t' + x._2.blsscore)
        )
  }

  def saveBedFilePerMotif(bedlines: RDD[(String, String)], outputDir: String, conf: Configuration, approxNumberOfKeys: Int = -1) : Long = {
    val outDir = if(outputDir.last == '/') outputDir else outputDir + "/";
    val parts = if(approxNumberOfKeys > 0) approxNumberOfKeys else bedlines.partitions.size

    val serdeserconf = new ConfigSerDeser(conf)
    val outfs = FileSystem.get(new URI(outDir), serdeserconf.get());

    val fs = FileSystem.get(new URI(outDir), serdeserconf.get());
    val outPath = new Path(outDir);
    if(fs.exists(outPath)) fs.delete(outPath, true);
    fs.mkdirs(outPath);

    val c = bedlines.partitionBy(new HashPartitioner(parts)).mapPartitions(it => {
      val fs = FileSystem.get(new URI(outDir), serdeserconf.get());
      val fileWriterMap: scala.collection.mutable.Map[String, OutputStream] = scala.collection.mutable.Map.empty[String, OutputStream];
      while(it.hasNext ) {
        val t = it.next();
        fileWriterMap.getOrElseUpdate(t._1, fs.create(new Path(outDir + t._1 + ".bed"))).write((t._2 + "\n").getBytes);
      }
      fileWriterMap.foreach(x => x._2.close());
      fileWriterMap.map(_._1).iterator // motif names
    }).count // makes sure its written to data

    fs.createNewFile(new Path(outDir + "_SUCCESS"));
    return c
  }

  val binary = bindir + "/motifIterator"
  val AlignmentBasedCommand = "--AB"
  val AlignmentFreeCommand = "--AF"
  val alphabets = Array("", "", "--twofoldAndN", "--fullIupac")
  def getCommand(mode: String, alignmentBased: Boolean, thresholdList: List[Float], alphabet: Int, maxDegen: Int, minMotifLen: Int, maxMotifLen: Int,
    useRC: Boolean, minBlsScore: Double, threads: Int, parquetOutput: String) : Seq[String] = {
    mode match {
      case "getMotifs" => tokenize( binary + " discovery - " + (if (alignmentBased) AlignmentBasedCommand else AlignmentFreeCommand) + " " + alphabets(alphabet)  + " --bls " +
                           thresholdList.mkString(",") + " --degen " + maxDegen + " --lengthrange " + minMotifLen + "," + maxMotifLen +
                           " --count" + " --threads " + threads + " --parquet " + parquetOutput + (if (useRC) "" else " --skipRC"))
      case "locateMotifs" => tokenize( binary + " match - " + (if (alignmentBased) AlignmentBasedCommand else AlignmentFreeCommand) + " --bls " +
                           thresholdList.mkString(",") + " --degen " + maxDegen + " --length " + (maxMotifLen - 1) + " --minbls " + minBlsScore + (if (useRC) "" else " --skipRC"))
      case _ => throw new Exception("invalid Mode")
    }
  }

  def filterMotifs(input: RDD[String], thresholdList: List[Float], fam_cutoff: Int, conf_cutoff:  Double) = {
    val tmp = input.flatMap(x => {
      val tmp = x.split("\t")
      var column = 0;
      while(column < thresholdList.size && !(tmp(column + 1).toInt > fam_cutoff && tmp(column + 1 + thresholdList.size).toDouble > conf_cutoff)) {
        column+=1;
      }
      if (column < thresholdList.size) Some(tmp(0) + "\t" + column)  else None
    })
    tmp
  }

  def getPermutations(s: String): List[String] = {
    def merge(ins: String, c: Char): Seq[String] =
      for (i <- 0 to ins.length) yield
        ins.substring(0, i) + c + ins.substring(i, ins.length)

    if (s.length() == 1)
      List(s)
    else
      getPermutations(s.substring(0, s.length - 1)).flatMap { p =>
        merge(p, s.charAt(s.length - 1))
      }
  }
  def getPermutation(s: String) : String = {
    var perm = s
    val indexes = List.range(0, s.length)
    val maxiter = 100
    var iter = 0
    while (perm == s && iter < maxiter) { // make sure it's not the same
      perm = ""
      var shuffled = scala.util.Random.shuffle(indexes)
      for (i <- shuffled) {
        perm += s(i)
      }
      iter += 1
    }
    return perm
  }

  def getRepresentative(s: String) : String = {
    val rc = reverseComplement(s)
    return if(rc < s) rc else s
  }
  def addPermutationPerMotif(input: String, partitions: Int, sc: SparkContext, thresholdList: List[Float], motifs: RDD[String], output: String) : Array[String] = {
    val outdir =  if(output.endsWith(("/"))) output.dropRight(1) + "-motif-perm-mapping" else output + "-motif-perm-mapping"
    be.ugent.intec.ddecap.tools.FileUtils.deleteRecursively(outdir)


    val allpreservedmotifs = sc.textFile(input, partitions).map(x => {
      val tmp = x.split("\t")
      var column = 0;
      var max = 0.0f
      while(column < thresholdList.size) {
        if (tmp(column + 1 + thresholdList.size).toFloat > max)
          max = tmp(column + 1 + thresholdList.size).toFloat
        column+=1;
      }
      (getRepresentative(tmp(0)), max.toFloat)
    })
    val permutations = motifs.flatMap(x => {
      val splt = x.split("\t")
      val prms = collection.SortedSet(getPermutations(splt(0)):_ *)
      prms.map(y => (getRepresentative(y), (getRepresentative(splt(0)) + "\t" + splt(1), getRepresentative(y) + "\t" + "0")))
    })
    // left outer join and then filter out motifs with (k, (v, None)) // assumes theres at least one permutation that isn't in here...
    val tmp = permutations.leftOuterJoin(allpreservedmotifs).map(x => {
                              if (x._2._2 == None) (x._1, (x._2._1, 0.0f))
                              else (x._1, (x._2._1, x._2._2.get.asInstanceOf[Float]))
                            }) // (permutation, ((motif+bls, perm+0), maxconf)) with none mapped to 0.0, ie not preserved
                          .map(x => (x._2._1._1, (x._2._2, x._2._1._2))) // (motif+bls, (maxconf, perm+0))
                          .reduceByKey((a, b) => if (a._1 < b._1) a else if (a._1 == b._1) {if (scala.util.Random.nextInt % 2 == 0) a else b; } else b)

    // WRITE TO FILE
    tmp.persist()
    info("perms with conf > 0.0: " + tmp.filter(_._2._1 != 0.0).count)
    tmp.map(x => (x._1, x._2._2)).map(x => x._1.split("\t")(0) + "\t" + x._2.split("\t")(0)).saveAsTextFile(outdir)
    val arr = tmp.map(x => (x._1, x._2._2)).flatMap(x => List(x._1, x._2)).sortBy(identity).collect
    tmp.unpersist()
    return arr;
  }

  def locateMotifs(families: RDD[String], mode: String, alphabet: Int, motifs: Broadcast[Array[String]], alignmentBased: Boolean,
    maxDegen: Int, maxMotifLen: Int, thresholdList: List[Float], minBlsScore: Double, useRC: Boolean) : RDD[(MotifKey, MotifInfo)] = {

    val preppedFamiliesAndMotifs = families.map(x => (x + motifs.value.mkString("\n") + "\n"))
    val motifWithLocations = preppedFamiliesAndMotifs.pipe(getCommand(mode, alignmentBased, thresholdList, alphabet, maxDegen, maxMotifLen - 1, maxMotifLen, useRC, minBlsScore, 1, ""))
    val locations = motifWithLocations.flatMap(x => {
      val split = x.split("\t")
      val rc = reverseComplement(split(0))
      val rep = if(useRC && rc < split(0)) rc else split(0)
      split(2).split(";").map(y => {
        val tmp = y.split("@")
        if (!useRC && tmp(1)(0) != '+')
          throw new Exception("should not happen, there is a neg location when not using the RC: " + x)
        (MotifKey(tmp(0), tmp(1).toInt), MotifInfo(rep, split(1).toFloat, tmp(1)(0) == '+')) //  (geneid, posingene) (motifrep, blsscore, strand)
      })
    })
    return locations
  }


  // info about data input:
        // iterateMotifs (c++ binary) outputs binary data, per motif this content is given:
        // 1 byte: length of motif
        // x bytes: motif content group in binary format, length depends on first byte (length) where there's 2 characters per byte
        // x bytes: motif itself in binary format
        // 1 byte: bls vector, first bit is 1 if the bls sscore of this motif in this family is higher then the first threshold, and so on for up to 8 thresholds
        // binary format:
          // 2 charactes per byte:
          //    4 bits per character:   T G C A
          //                            x x x x -> 1 if that letter is in the iupac letter, 0 if not
          //                      ie A: 0 0 0 1
          //                      ie G: 0 1 0 0
          //                      ie M: 0 0 1 1 // A or C

      // this is formatted in a key value pair as follows:
      // key: array[byte] -> first byte of length + motif content group
      // value: (array[byte], byte) -> the content of the motif itself (without the length! as this is already in the key) + the bls byte
  def iterateMotifsPerOrthoGroup(input: RDD[String], conf: Configuration, mode: String, alignmentBased: Boolean, alphabet: Int,
    maxDegen: Int, minMotifLen: Int, maxMotifLen: Int,
    thresholdList: List[Float], useRC: Boolean, cpus: Int, outputuri: String) : String = {
      val parquetOutput = outputuri + ".parquet"
      val fs = FileSystem.get(new URI(parquetOutput), conf);
      val path = new Path(parquetOutput);
      if(fs.exists(path)) fs.delete(path, true);
      fs.mkdirs(path)

      val rdd = new org.apache.spark.StringPipedRDD( input , getCommand(mode, alignmentBased, thresholdList, alphabet,
        maxDegen, minMotifLen, maxMotifLen, useRC, 0, cpus, parquetOutput), "motifIterator")

      rdd.count // actually performs the thing, since it saves the data in intermediate parquet files
      return parquetOutput
  }
}
