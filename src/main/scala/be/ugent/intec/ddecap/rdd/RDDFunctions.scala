package be.ugent.intec.ddecap.rdd

import org.apache.spark.rdd.RDD
import org.apache.log4j._
import org.apache.hadoop.io.{NullWritable, BytesWritable, LongWritable}
import org.apache.hadoop.conf.Configuration
import collection.immutable.HashMap
import be.ugent.intec.ddecap.spark.ConfigSerDeser
import scala.collection.immutable.HashSet
import be.ugent.intec.ddecap.dna.BlsVector
import be.ugent.intec.ddecap.dna.BlsVectorFunctions._
import be.ugent.intec.ddecap.dna.BinaryDnaStringFunctions._
import be.ugent.intec.ddecap.spark.DnaStringPartitioner
import be.ugent.intec.ddecap.tools.FileUtils.deleteRecursively
import scala.collection.mutable.ListBuffer
import be.ugent.intec.ddecap.dna.LongEncodedDna._
import be.ugent.intec.ddecap.dna.{ImmutableDnaPair, ImmutableDnaWithBlsVectorByte, ImmutableDnaWithBlsVector}
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.SparkContext
import org.apache.spark.storage.StorageLevel
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.{DataFrame, SparkSession, Encoder, Encoders}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import scala.collection.mutable.ArrayBuffer
import java.net.URI
import be.ugent.intec.ddecap.tools.Distribution._
import be.ugent.intec.ddecap.tools.AbstractDistribution

object RDDFunctions {
  val logger = Logger.getLogger("be.ugent.intec.ddecap.rdd.RDDFunctions");
  type ImmutableDna = Long
  // type BlsVector = Array[Int];

  def countAndCollectMotifs(input: RDD[(ImmutableDna, ImmutableDnaWithBlsVector)], partitions: Int) : RDD[(ImmutableDna, HashMap[ImmutableDna, BlsVector])] = {
    // per content group, we collect all motifs, per motif in this group we combine these together by adding the bls vector
    input.combineByKey(
        (p: ImmutableDnaWithBlsVector) => { // convert input value to output value
          HashMap((p.motif -> p.vector))
        },
        (map:HashMap[ImmutableDna, BlsVector], p:ImmutableDnaWithBlsVector) => { // merge an input value with an already existing output value
          if(map.contains(p.motif)) {
            map(p.motif).addVector(p.vector)
            map
          } else {
            map + (p.motif -> p.vector)
          }
        },
        (map1:HashMap[ImmutableDna, BlsVector], map2:HashMap[ImmutableDna, BlsVector]) => { // merge two existing output values together
          map1.merged(map2)({
            case ((k,v1:BlsVector),(_,v2:BlsVector)) => (k,v1.addVector(v2))
          })
        }, new DnaStringPartitioner(partitions), false)
  }
  def countAndCollectUniqueMotifs(input: RDD[(ImmutableDna, ImmutableDnaWithBlsVector)], partitions: Int) : RDD[(ImmutableDna, Vector[(ImmutableDna, BlsVector)])] = {
    // per content group, we collect all motifs, per motif in this group we combine these together by adding the bls vector
    input.combineByKey(
        (p: ImmutableDnaWithBlsVector) => { // convert input value to output value
          Vector((p.motif, p.vector))
        },
        (v:Vector[(ImmutableDna, BlsVector)], p:ImmutableDnaWithBlsVector) => { // merge an input value with an already existing output value
          v :+ (p.motif, p.vector)
        },
        (v1:Vector[(ImmutableDna, BlsVector)], v2:Vector[(ImmutableDna, BlsVector)]) => { // merge two existing output values together
          v1 ++ v2
        }, new DnaStringPartitioner(partitions), false)
  }

  def countPerMotifAndPartitionPerGroup(input: RDD[(ImmutableDnaPair, BlsVector)], partitions: Int) : RDD[(ImmutableDnaPair, BlsVector)] = {
    input.reduceByKey(new DnaStringPartitioner(partitions), (a:BlsVector, b:BlsVector) => a.addVector(b))
  }


  def processGroups(input: RDD[(ImmutableDnaPair, BlsVector)],
        thresholdList: List[Float],
        backgroundModelCount: Int, similarityScore: Int,
        familyCountCutOff: Int, confidenceScoreCutOff: Double,
        emitRandomLowConfidenceScoreMotifs: Int = 0) :
        RDD[(ImmutableDna, BlsVector, List[Float], ImmutableDna)] = {
    input.mapPartitions(it => {
      val retlist = ListBuffer.empty[(ImmutableDna, BlsVector, List[Float], ImmutableDna)]
      val alldata = scala.collection.mutable.HashMap.empty[ImmutableDna, HashMap[ImmutableDna, BlsVector]] // cause they are not sorted!
      while(it.hasNext) {
        val p : (ImmutableDnaPair, BlsVector) = it.next
          if(!alldata.contains(p._1.group)) // add group if needed
            alldata += (p._1.group -> HashMap.empty[ImmutableDna, BlsVector])
          alldata(p._1.group) = alldata(p._1.group) + (p._1.motif -> p._2)
      }

      for ((key,data) <- alldata) {
        // logger.info("current group " + LongToDnaString(key) + " with " + data.size + " motifs in group")
        val groupIsItsOwnRC = isGroupItsOwnRc(key);
        val len = getDnaLength(key)
        val rnd = new scala.util.Random
        val median : BlsVector = getMedianPerThresholdFromHashMap(key, data, thresholdList.size)
        for( d <- data) { // for each motif calculate every F(Ti) and corresponding C(Ti)
          val conf_score_vector = Array.fill(thresholdList.size)(0.0f)
          var thresholds_passed = false;
          var emit_rnd = false;
          for(t <- 0 until thresholdList.size) {  // for every Threshold Ti:
            val family_count_t = d._2.getThresholdCount(t) // F(Ti)
            val family_count_bg_t = median.getThresholdCount(t).toFloat
            conf_score_vector(t) = if(family_count_t > family_count_bg_t) (family_count_t - family_count_bg_t) / family_count_t else 0.0f; // C(Ti)
            if(family_count_t > familyCountCutOff && conf_score_vector(t) > confidenceScoreCutOff) {
              thresholds_passed = true;
            } else {
              if (emitRandomLowConfidenceScoreMotifs > 0 && rnd.nextInt(emitRandomLowConfidenceScoreMotifs) == 0){
                emit_rnd = true;
              }
            }
          }
          if(thresholds_passed) {
            if(groupIsItsOwnRC) { // avoid emitting motifs twice in this group
              if(isRepresentative(d._1, len)) {
                retlist += ((d._1, d._2, conf_score_vector.toList, key))
              }
            } else {
              retlist += ((d._1, d._2, conf_score_vector.toList, key))
            }
          } else if (emit_rnd) {
            retlist += ((d._1, d._2, conf_score_vector.toList, key))
          }
        }
      }
      retlist.iterator
    })
  }

def processHashMapGroups(input: RDD[(ImmutableDna, HashMap[ImmutableDna, BlsVector])],
    thresholdList: List[Float],
    backgroundModelCount: Int, similarityScore: Int,
    familyCountCutOff: Int, confidenceScoreCutOff: Double,
    emitRandomLowConfidenceScoreMotifs: Int = 0,
    motiflist: HashSet[String]) :
    RDD[(ImmutableDna, BlsVector, List[Float], ImmutableDna)] = {
    input.flatMap(x => { // x is an iterator over the motifs+blsvector in this group
      val key = x._1
      val groupIsItsOwnRC = isGroupItsOwnRc(key);
      val len = getDnaLength(key)
      val data = x._2
      val rnd = new scala.util.Random
      val median : BlsVector = getMedianPerThresholdFromHashMap(key, data, thresholdList.size)
      val retlist = ListBuffer[(ImmutableDna, BlsVector, List[Float], ImmutableDna)]()
      // logger.info("current group " + LongToDnaString(key) + " with " + data.size + " motifs in group")
      for( d <- data) { // for each motif calculate every F(Ti) and corresponding C(Ti)
        val conf_score_vector = Array.fill(thresholdList.size)(0.0f)
        var thresholds_passed = false;
        var emit_rnd = false;
        for(t <- 0 until thresholdList.size) {  // for every Threshold Ti:
          val family_count_t = d._2.getThresholdCount(t) // F(Ti)
          val family_count_bg_t = median.getThresholdCount(t).toFloat
          conf_score_vector(t) = if(family_count_t > family_count_bg_t) (family_count_t - family_count_bg_t) / family_count_t else 0.0f; // C(Ti)
          if(motiflist.isEmpty){
            if(family_count_t > familyCountCutOff && conf_score_vector(t) > confidenceScoreCutOff) {
              thresholds_passed = true;
            } else if (emitRandomLowConfidenceScoreMotifs > 0 && rnd.nextInt(emitRandomLowConfidenceScoreMotifs) == 0){
              emit_rnd = true;
            }
          } else {
            val motif = LongToDnaString(d._1, getDnaLength(key))
            val rc = reverseComplement(motif)
            if (motiflist.contains(motif) || motiflist.contains(rc)) {
              thresholds_passed = true;
            } else if (!(family_count_t > familyCountCutOff && conf_score_vector(t) > confidenceScoreCutOff) &&
                        emitRandomLowConfidenceScoreMotifs > 0 && rnd.nextInt(emitRandomLowConfidenceScoreMotifs) == 0){
              emit_rnd = true;
            }
          }
        }
        if(thresholds_passed) {
          if(groupIsItsOwnRC) { // avoid emitting motifs twice in this group
            if(isRepresentative(d._1, len)) {
              retlist += ((d._1, d._2, conf_score_vector.toList, key))
            }
          } else {
            retlist += ((d._1, d._2, conf_score_vector.toList, key))
          }
        } else if (emit_rnd) {
          retlist += ((d._1, d._2, conf_score_vector.toList, key))
        }
      }
      retlist
    })
  }


  def processVectorGroups(input: RDD[(ImmutableDna, Vector[(ImmutableDna, BlsVector)])],
      thresholdList: List[Float],
      backgroundModelCount: Int, similarityScore: Int,
      familyCountCutOff: Int, confidenceScoreCutOff: Double,
      motiflist: HashSet[String],
      confidenceType: Int = 0,
      emitRandomLowConfidenceScoreMotifs: Int = 0,
      plotDir: String, conf: Configuration) :
      RDD[(ImmutableDna, BlsVector, List[Float], ImmutableDna)] = {
    val serdeserconf = new ConfigSerDeser(conf)
    if(confidenceType == 0)
      logger.info("using confidence score as cutoff")
    else
      logger.info("fitting distribution and using -log10(1-probability) as cutoff")


    input.flatMap(x => { // x is an iterator over the motifs+blsvector in this group
      val key = x._1
      val groupIsItsOwnRC = isGroupItsOwnRc(key);
      val len = getDnaLength(key)
      val data = x._2
      val rnd = new scala.util.Random
      val retlist = ListBuffer[(ImmutableDna, BlsVector, List[Float], ImmutableDna)]()
      var median : BlsVector = new BlsVector(Array.fill(len)(0)) // empty data
      var dists = ListBuffer.empty[AbstractDistribution] // empty data
      if(confidenceType == 0) {
        median = getMedianPerThresholdFromVector(key, data, thresholdList.size)
        logger.info(LongToDnaString(key) + "\t" + data.size + "\tmedians:\t" + median)
      } else
        dists = getDistributionModelPerThresholdFromVector(key, data, thresholdList.size, plotDir, serdeserconf)

      for( d <- data) { // for each motif calculate every F(Ti) and corresponding C(Ti)
        val conf_score_vector = Array.fill(thresholdList.size)(0.0f)
        var thresholds_passed = false;
        var emit_rnd = false;
        for(t <- 0 until thresholdList.size) {  // for every Threshold Ti:
          val family_count_t = d._2.getThresholdCount(t) // F(Ti)
          if(confidenceType == 0) {
            val family_count_bg_t = median.getThresholdCount(t).toFloat
            conf_score_vector(t) = if(family_count_t > family_count_bg_t) (family_count_t - family_count_bg_t) / family_count_t else 0.0f; // C(Ti)
          } else {
            conf_score_vector(t) = dists(t).negLog10InvCumulativeProbability(family_count_t).toFloat
          }

          if(motiflist.isEmpty){
            if(family_count_t > familyCountCutOff && conf_score_vector(t) > confidenceScoreCutOff) {
              thresholds_passed = true;
            } else if (emitRandomLowConfidenceScoreMotifs > 0 && rnd.nextInt(emitRandomLowConfidenceScoreMotifs) == 0){
              emit_rnd = true;
            }
          } else {
            val motif = LongToDnaString(d._1, getDnaLength(key))
            val rc = reverseComplement(motif)
            if (motiflist.contains(motif) || motiflist.contains(rc)) {
              thresholds_passed = true;
            } else if (!(family_count_t > familyCountCutOff && conf_score_vector(t) > confidenceScoreCutOff) &&
                        emitRandomLowConfidenceScoreMotifs > 0 && rnd.nextInt(emitRandomLowConfidenceScoreMotifs) == 0){
              emit_rnd = true;
            }
          }
        }

        if(thresholds_passed) {
          if(groupIsItsOwnRC) { // avoid emitting motifs twice in this group
            if(isRepresentative(d._1, len)) {
              retlist += ((d._1, d._2, conf_score_vector.toList, key))
            }
          } else {
            retlist += ((d._1, d._2, conf_score_vector.toList, key))
          }
        } else if (emit_rnd) {
          retlist += ((d._1, d._2, conf_score_vector.toList, key))
        }
      }
      retlist
    })
  }

  def processMotifsInDataframe(spark: SparkSession, sc: SparkContext, motifs: DataFrame, familyCountCutOff: Int, confidenceScoreCutOff: Double, outputFolder: String) = {
    import spark.implicits._
// implicit encoders

    deleteRecursively(outputFolder)
    sc.setCheckpointDir(outputFolder + "/checkpoints")
    val thresholds = motifs.schema.length - 2

    val medianKs = motifs.select("group").groupBy("group").count().map(x => {
       val perms = getNumberOfPermutationsFromString(x.getString(0))
       val nr = (perms + 1) / 2
       (x.getString(0), (if (x.getLong(1) > nr) (x.getLong(1) - nr - 1)  else -1))
     }).toDF("group", "row_number").checkpoint()

    val medians = ArrayBuffer.empty[DataFrame]
    for (blsThreshold <- 0 until thresholds) {
      @transient val blsThresholdOrder  = Window.partitionBy("group").orderBy("bls" + blsThreshold)
      val ordered = motifs.select("group", "bls" + blsThreshold).withColumn("row_number", row_number().over(blsThresholdOrder) )
      medians += ordered.join(broadcast(medianKs), Seq("group", "row_number"), "inner").select(col("group"), col("bls" + blsThreshold).as("median" + blsThreshold)).checkpoint()
      logger.info("gathered medians for BLS threshold " + blsThreshold)
    }
    val allmedians = medians.reduce(_.join(_, Seq("group"), "outer")).checkpoint()

 //
    val output = motifs.filter(Range(0, thresholds).map(x => motifs("bls" + x) > familyCountCutOff).reduce(_||_)).join(broadcast(allmedians), Seq("group"), "left").flatMap(row => {
      val conf_scores = ArrayBuffer.empty[Float]
      val counts = ArrayBuffer.empty[Int]
      var passes = false;
      for (i <- 0 until thresholds) {
        val observed_count  = row.getInt(2 + i);
        val median_count : Float = if(row.isNullAt(thresholds + 2 + i)) 0 else row.getInt(thresholds + 2 + i);
        counts += observed_count
        conf_scores += (if (observed_count > median_count) (observed_count - median_count) / observed_count else 0.0f);
        if(conf_scores.last > confidenceScoreCutOff && observed_count > familyCountCutOff) passes = true;
      }
      if(passes) Some(row.getString(1), counts.toArray, conf_scores.toArray) else None
    }).select(
      Seq(col("_1").as("motif")) ++
      Range(0, thresholds).map(i => col("_2")(i).as("bls" + i)) ++
      Range(0, thresholds).map(i => col("_3")(i).as("conf" + i)) : _*
    )
    output.write.parquet(outputFolder + "/output.parquet")

    // clean up output folder
    val conf = new Configuration(sc.hadoopConfiguration)
    val cpPath = new Path(outputFolder + "/checkpoints")
    val tmpPath = new Path(outputFolder + "/output.parquet")
    val outPath = new Path(outputFolder)
    val fs = FileSystem.get(new URI(outputFolder), conf);
    // rm checkpoints folder
    if(fs.exists(cpPath))
        fs.delete(cpPath, true)
    // mv all files to outPath and remove tmpPath
    fs.listStatus(tmpPath).filter(_.isFile).foreach(x => fs.rename(x.getPath, outPath.suffix("/" + x.getPath.getName)))
    if(fs.exists(tmpPath) && fs.listStatus(tmpPath).filter(_.isFile).length == 0)
        fs.delete(tmpPath, true)
  }

}
