package be.ugent.intec.ddecap.spark

// import spark.implicits._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Dataset, DataFrame, SparkSession}
import scala.collection.immutable.ListMap
import org.apache.spark.rdd.RDD
import be.ugent.intec.ddecap.dna.LongEncodedDna._
import be.ugent.intec.ddecap.dna.{BlsVector, ImmutableDnaWithBlsVector}
import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._

object BlsParquet {
  val logger = Logger.getLogger("be.ugent.intec.ddecap.spark.BlsParquet");

  def countPerMotif(input: DataFrame) : DataFrame = {
    input.groupBy("group", "motif").agg(sum("bls0").cast(org.apache.spark.sql.types.IntegerType).as("bls0"), Range(1, input.schema.length - 2).map(x => sum("bls" + x).cast(org.apache.spark.sql.types.IntegerType).as("bls" + x)) :_*)
  }

  def filterMotifs(spark: SparkSession, input: DataFrame, thresholdList: List[Float], fam_cutoff: Int, conf_cutoff:  Double) : Dataset[String] = {
    import spark.implicits._
    input.flatMap(x => {
      var idx = 0
      while(idx < thresholdList.size && !(x.getInt(1+idx) > fam_cutoff && x.getFloat(1+thresholdList.size+idx) > conf_cutoff))
        idx += 1
      if(idx < thresholdList.size) Some(x.getString(0) + "\t" + idx) else None
    })
  }

  def dataframeToRDD(motifs: DataFrame) : RDD[(ImmutableDna, ImmutableDnaWithBlsVector)] = {
    return motifs.rdd
                 .map(x => (DnaStringToLongWithLength(x.getString(0)),
                            ImmutableDnaWithBlsVector(DnaStringToLong(x.getString(1)),
                                                      new BlsVector(x.toSeq.drop(2).toArray.map(_.asInstanceOf[Int])))
                           )
                    )
  }



}
