package be.ugent.intec.ddecap.dna

import org.apache.log4j._

import scala.annotation.tailrec
import scala.collection.immutable.HashMap
import scala.collection.mutable.ListBuffer
import be.ugent.intec.ddecap.dna.LongEncodedDna._
import be.ugent.intec.ddecap.spark.ConfigSerDeser
import org.apache.hadoop.fs.{FileSystem, Path}
import java.net.URI
import java.io.IOException
import  be.ugent.intec.ddecap.tools.Distribution._
import  be.ugent.intec.ddecap.tools.AbstractDistribution
import org.nspl._
import org.nspl.data.HistogramData
import org.nspl.awtrenderer._
// import sim.util.distribution.{Arithmetic, NegativeBinomial, Probability}
// import org.apache.commons.math3.distribution.{AbstractIntegerDistribution, PascalDistribution, BinomialDistribution, PoissonDistribution}

object BinaryDnaStringFunctions {

  private final val byteToAscii = Array(' ', 'A', 'C', 'M', 'G', 'R', 'S', 'V', 'T', 'W', 'Y', 'H', 'K', 'D', 'B', 'N')
  val logger = Logger.getLogger("be.ugent.intec.ddecap.dna.BinaryDnaStringFunctions");
  type ImmutableDna = Long
  // type BlsVector = Array[Int];
  private val similarityCountfactor = 5;

  def generateBackgroundModel(key: ImmutableDna, backgroundModelCount: Int, similarityScore: Int): ListBuffer[ImmutableDna] = {
    similarityScore match {
      case 0 => {
        generateDissimilarBackgroundModel(key, backgroundModelCount, binaryHammingSimilarityScore)
      }
      case 1 => {
        generateDissimilarBackgroundModel(key, backgroundModelCount, hammingSimilarityScore)
      }
      case 2 => {
        generateDissimilarBackgroundModel(key, backgroundModelCount, levenshteinSimilarityScore)
      }
      case _ => {
        generateBackgroundModel(key, backgroundModelCount)
      }
    }
  }

  private def generateDissimilarBackgroundModel(key: ImmutableDna, backgroundModelCount: Int, similarityScore: SimilarityScoreType => Long) : ListBuffer[ImmutableDna] = {
    val bgmodel = generateBackgroundModel(key, similarityCountfactor*backgroundModelCount)
    val orderedbgmodel = orderByDissimilarityScore(bgmodel, getDnaLength(key), similarityScore)
    orderedbgmodel.take(backgroundModelCount)
  }


  private def fact(n: Int) : Int = {
    var f = 1
    for (i <- 2 to n) {
      f = f * i
    }
    return f
  }

  private final val bgCountFactor = 4
  private final val bgCountMinimum = 1000
  def getNumberOfPermutations(motifgroup: ImmutableDna): Int = {
    val perm = getNumberOfPermutationsFromCharArray(getDnaContent(motifgroup))
    return perm
  }
  private def getNumberOfPermutationsFromCharArray(chars: ListBuffer[Long]): Int = {
    val charcounts = chars.groupBy(identity).map(x => (x._1, x._2.size))
    var tmp = 1
    for (c <- charcounts) {
    	tmp = tmp * fact(c._2)
    }
    return fact(chars.size)/tmp
  }
  def getNumberOfPermutationsFromString(group: String): Int = {
    val charcounts = group.groupBy(identity).map(x => (x._1, x._2.size))
    var tmp = 1
    for (c <- charcounts) {
      tmp = tmp * fact(c._2)
    }
    return fact(group.size)/tmp
  }

  private def getBgCountBasedOnNumberOfPermutations(chars: ListBuffer[Long]): Int = {
    val permutations = getNumberOfPermutationsFromCharArray(chars)
    return Math.max(bgCountMinimum,bgCountFactor*permutations)
  }

  private def generateBackgroundModel(key: ImmutableDna, backgroundModelCount: Int) : ListBuffer[ImmutableDna] = {
    // generate x permutations of this key motif
    val ret = ListBuffer[ImmutableDna]()
    val chars = getDnaContent(key)
    val bgcount = if(backgroundModelCount > 0) backgroundModelCount else (getBgCountBasedOnNumberOfPermutations(chars))
    for (tmp <- 0 until bgcount) {
      val shuffledIdx = util.Random.shuffle[Int, IndexedSeq](0 until chars.size)
      // random order of chars
      var newdata: Long = 0
      var i = 0
      for (cidx <- shuffledIdx) {
        val c = chars(cidx)
        newdata |= (c << (60 - (4*i))).toLong // 60 here since no length
        i+= 1
      }
      // logger.info("bg long: " + newdata.toBinaryString)
      ret += newdata;
    }
    ret
  }

  def bitCount(x: Long) : Long =  {
    var i = x
    i - ((i >>> 1) & 0x5555555555555555L);
    i = (i & 0x3333333333333333L) + ((i >>> 2) & 0x3333333333333333L);
    i = (i + (i >>> 4)) & 0x0f0f0f0f0f0f0f0fL;
    i = i + (i >>> 8);
    i = i + (i >>> 16);
    i = i + (i >>> 32);
    return i & 0x7f;
  }
  def binaryHammingSimilarityScore(motifdata: SimilarityScoreType) : Long = {
    var ret = bitCount((motifdata.motifa^motifdata.motifb) >> ((16-motifdata.len)<<2)) // shift to right in order to eliminate problems with leftover 1s in that part
    ret.toInt
  }
  def levenshteinSimilarityScore(motifdata: SimilarityScoreType) : Long = {
    var a = (0 until motifdata.len).toList
    var b = (0 until motifdata.len).toList

    ((0 to b.size).toList /: a)((prev, x) =>
     (prev zip prev.tail zip b).scanLeft(prev.head + 1) {
         case (h, ((d, v), y)) => math.min(math.min(h + 1, v + 1), d + (if (((motifdata.motifa >> (60 -4*x)) & 0xf) == ((motifdata.motifb >> (60 -4*y)) & 0xf)) 0 else 1))
       }).last
  }
  def hammingSimilarityScore(motifdata: SimilarityScoreType) : Long = {
    var count = 0L
    var xor = (motifdata.motifa ^ motifdata.motifb) >> ((16-motifdata.len)<<2);
    for (i <- 0 until motifdata.len) {
      if ( (xor & 0xf) > 0) {
        count += 1
      }
      xor = xor >> 4;
    }
    count
  }

  def orderByDissimilarityScore(bgmodel: ListBuffer[ImmutableDna], motiflen: Int, similarityScore: SimilarityScoreType => Long) : ListBuffer[ImmutableDna] = {
    val tmp = bgmodel.map(x => {
      val s : Double = bgmodel.map(y => similarityScore(SimilarityScoreType(x,y, motiflen))).sum
      (x, s/bgmodel.size)
    })
    tmp.sortBy(-_._2).map(_._1)
  }

  def chooseRandomPivot(arr: Seq[Int]): Int = arr(scala.util.Random.nextInt(arr.size))
  @tailrec final def findKMedian(arr: Seq[Int], k: Int): Int = {
      val a = chooseRandomPivot(arr)
      val (s, b) = arr partition (a >)
      if (s.size == k) a
      // The following test is used to avoid infinite repetition
      else if (s.isEmpty) {
          val (s, b) = arr partition (a ==)
          if (s.size > k) a
          else findKMedian(b, k - s.size)
      } else if (s.size < k) findKMedian(b, k - s.size)
      else findKMedian(s, k)
  }
  def findMedian(arr: Seq[Int]) = findKMedian(arr, (arr.size - 1) / 2)


  def getMedianPerThresholdFromHashMap(key: ImmutableDna, data: HashMap[ImmutableDna, BlsVector], thresholdListSize: Int) : BlsVector = {
    val chars = getDnaContent(key)
    val perms = getNumberOfPermutationsFromCharArray(chars)
    if(perms == 1) return data.head._2
    else {
      val nr = (perms + 1) / 2
      val arr = Array.fill(thresholdListSize)(0)
      if(data.size > nr) {
        val k = data.size - nr - 1
        for (i <- 0 until thresholdListSize) {
          arr(i) = findKMedian(data.toSeq.map(_._2.getThresholdCount(i)), k)
        }
      }
      new BlsVector(arr)
    }
  }

  def getMedianPerThresholdFromVector(key: ImmutableDna, data: Vector[(ImmutableDna, BlsVector)], thresholdListSize: Int) : BlsVector = {
    val chars = getDnaContent(key)
    val perms = getNumberOfPermutationsFromCharArray(chars)
    logger.info("motifgroup '" + LongToDnaString(key) + "' has " + perms + " permutations")
    if(perms == 1) return data.head._2
    else {
      val nr = (perms + 1) / 2
      val arr = Array.fill(thresholdListSize)(0)
      if(data.size > nr) {
        val k = data.size - nr - 1
        for (i <- 0 until thresholdListSize) {
          arr(i) = findKMedian(data.toSeq.map(_._2.getThresholdCount(i)), k)
        }
      }
      new BlsVector(arr)
    }
  }


// TODO rewrite this whole function so that a single for loop over thresholds
  def getDistributionModelPerThresholdFromVector(key: ImmutableDna, data: Vector[(ImmutableDna, BlsVector)], thresholdListSize: Int, plotDir : String = "", serdeserconf: ConfigSerDeser) : ListBuffer[AbstractDistribution] = {
    var dists = ListBuffer.empty[AbstractDistribution]
    val chars = getDnaContent(key)
    val perms = getNumberOfPermutationsFromCharArray(chars)
    logger.info("motifgroup '" + LongToDnaString(key) + "' has " + perms + " permutations")
    for (i <- 0 until thresholdListSize) {
      val zeros = perms - data.length + data.map(x => x._2.list(i)).filter(x => x == 0).length
      val zeroInflated : Double = zeros / perms.toDouble // set to 0.0 when not using zero inflated model
      val mean = if(perms == 1) data.head._2.list(i).toDouble
                 else if (data.filter(x => x._2.list(i) > 0).length == 0) 0.0
                 else data.filter(x => x._2.list(i) > 0).map(x => x._2.list(i)).sum.toDouble / (perms - zeros)
      val variance = if (perms == 1 || data.filter(x => x._2.list(i) > 0).length == 0) 0.0
                     else data.filter(x => x._2.list(i) > 0).map(x => Math.pow(x._2.list(i) - mean, 2)).sum.toDouble / (perms - zeros)

      dists += modelDistribution(mean, variance, zeroInflated)
      if(! plotDir.isEmpty()) {
        val groupId = LongToDnaString(key);
        val splits = 100
        val plot_N = 200
        logger.info("plotting histogram for " + groupId + "@BlsT" + i)

        val nr = (perms + 1) / 2
        val k = data.length - nr - 1
        val median = if(data.size > nr) findKMedian(data.toSeq.map(_._2.getThresholdCount(i)), k) else 0
        logger.info("median: " + median + ", mean: " + mean + ", variance: " + variance + ", zeroInflated: " + zeroInflated + " => modelled dist is " + dists(i).toString())

        val current_data : Seq[Double] = Seq.fill(perms - data.size)(0.0) ++ data.map(x => x._2.list(i).toDouble)
        val max = current_data.max
        val hist = xyplot(
             HistogramData(current_data, splits) -> bar()
           )(
             par(
               main = groupId + "@BlsT" + i + "; median: " + median + ", mean: " + mean + ", variance: " + variance,
               xlab = "# families",
               ylab = "freq.",
               xlim = Some(0d, max))
           )

        // MODELED CURVE + RANGE
        val fitCurve = ListBuffer[(Double, Double)]()
        val plotmin : Int = Math.max(0,(mean - 3 * Math.sqrt(variance)).toInt)
        val plotmax : Int = Math.max(1,(mean + 5 * Math.sqrt(variance)).toInt)
        val plotstep : Int = Math.max(1, ((plotmax - plotmin) / plot_N).toInt)
        for(x <- plotmin to plotmax by plotstep)
          fitCurve += ((x.toDouble, dists(i).cumulativeProbability(x)))
        val fit = xyplot(fitCurve.toList -> line())(
                 par(
                   main = groupId + "@BlsT" + i + "; " + dists(i).toString(),
                   xlab = "# families",
                   ylab = "cum prob.",
                   // ylim = Some(0d, 1),
                   xlim = Some(0d, max))
               )
  			try {
          val fs = FileSystem.get(new URI(plotDir), serdeserconf.get());
          val arr = pngToByteArray(group(hist, fit, VerticalStack()).build)
          val stream = fs.create(new Path(plotDir + "/plot-" + groupId + "-BlsT" + i + ".png"))
          stream.write(arr)
          stream.close()
        } catch {
          case e: IOException => logger.info(e.getMessage);
  			}
      }
    }

    return dists;
  }
}
