package be.ugent.intec.ddecap.dna
import collection.immutable.HashMap

final case class SimilarityScoreType(motifa: Long,  motifb: Long, len: Int);
final case class ImmutableDnaPair(motif: Long, group: Long);
final case class ImmutableDnaWithBlsVectorByte(motif: Long, blsbyte: Byte);
final case class ImmutableDnaWithBlsVector(motif: Long, vector: BlsVector);
final case class MotifKey(geneid: String, posingene: Int) extends Ordered[MotifKey] {
  def compare(that: MotifKey): Int = if(this.geneid == that.geneid) this.posingene - that.posingene else this.geneid.compare(that.geneid)
}
final case class MotifInfo(motifrep: String, blsscore: Float, strand: Boolean);
final case class GeneInfo(fasta:String, chromosome: String, isFwStrand: Boolean, start: Int, stop: Int);
final case class GenomePosition(fasta:String, chromosome: String, position: PositionData)
final case class PositionData(posingenome: Int, posingene: Int)
final case class MotifBedInfo(gene_strand: String, motif: String, blsscore: Float, geneid: String, motif_strand: Boolean)

//     (if(x._2._1._2) "+" else "-", // strand
//      x._2._2._2._1, // motif
//      x._2._2._2._2, // bls score
//      x._1, // geneid
//      x._2._2._2._3 // motif strand
//      )
