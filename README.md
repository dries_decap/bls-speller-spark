# bls-speller-spark

## Installation

- install [Apache Spark](https://spark.apache.org/)
- install [sbt](https://www.scala-sbt.org/)
- build a [suffixtree](https://bitbucket.org/dries_decap/suffixtree-motif-speller/src/master/) executable using `make`

- requires MASON library (https://cs.gmu.edu/~eclab/projects/mason/) in ``lib/`` folder
```bash
mkdir lib
cd lib/
wget https://cs.gmu.edu/~eclab/projects/mason/mason.20.jar
```
- build bls-speller-spark with
```bash
sbt assembly
```

## Scripts
Read about run scripts [here](https://bitbucket.org/dries_decap/bls-speller-spark/src/master/scripts/)
