
set terminal pdfcairo font "Helvetica, 14" size 4,4
set output "after_with_bls95_conf90.pdf"

#plot 'bls95_conf90.tsv' using 2:3 with points pt 7
set title "nr species vs nr motifs found"
set xlabel "# species in gene family"
set ylabel "# motifs found with bls >=.95 and conf >=.9"
plot 'bls95_conf90.data' u 1:2:3 w points lt 1 pt 6 ps variable t ""
