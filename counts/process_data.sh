#!/bin/bash
N=100
awk -v N=$N 'BEGIN{min=99999;max=0}{a[NR]=$2;b[NR]=$3; if($3>max){max=$3} if($3<min){min=$3}} END{box=(max-min)/N; for(i in a) print a[i]"\t"int(b[i]/box)*box}' <&0 | sort | uniq -c | awk '{print $2"\t"$3"\t"log($1)/2}'
