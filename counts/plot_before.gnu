
set terminal pdfcairo font "Helvetica, 14" size 4,4
set output "before_with_bls95.pdf"

#plot 'bls95.tsv' using 2:3 with points pt 7
set yrange [0:50000000]
set title "nr species vs nr motifs found"
set xlabel "# species in gene family"
set ylabel "# motifs found with bls >=.95"
plot 'bls95.data' u 1:2:3 w points lt 1 pt 6 ps variable t ""
