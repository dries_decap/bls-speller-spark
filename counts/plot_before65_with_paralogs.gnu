
set terminal pdfcairo font "Helvetica, 14" size 4,4
set output "before_with_bls65_withp.pdf"

#plot 'bls95.tsv' using 2:3 with points pt 7
set yrange [0:50000000]
set xrange [3:17]
set xtics offset 1 3,1,16
set cbrange [1:10]
set title "gene family impact"
set xlabel "# species in gene family"
set ylabel "# motifs found with bls >=.65"
set cblabel "# average paralogs per species in gene family"
plot 'bls65_with_color.data' u 1:2:3 w points lt 1 pt 7 palette ps 0.2 t ""
