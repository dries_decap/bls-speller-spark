#!/bin/bash

. bls_config.sh

# THIS SHOULD REMAIN THE SAME
v=$BLS_VERSION
jar=${lib}/bls-speller-assembly-${v}.jar
jars=${jar}
class=be.ugent.intec.ddecap.BlsSpeller
memory="--executor-memory ${JOB2_EXECMEM} --num-executors ${JOB2_EXECUTORS} --executor-cores ${JOB2_CPUS} --conf spark.task.cpus=${JOB2_CPUS} --conf spark.executor.memoryOverhead=${JOB2_OVERHEAD_MEM}"
sparksubmit="spark-submit --master yarn --conf spark.ui.showConsoleProgress=true ${memory} --conf spark.yarn.stagingDir=$STAGING_DIR --jars ${jars} --class ${class}"
PARTITIONS=$(( JOB2_EXECUTORS * 10 )) # should depend on % of conf
if [[ ${use_gene_pos} != "false" ]]; then
	gene_pos="--gene_pos"
fi
if [[ ${conf_percent} == "100" ]]; then
	conf_fraction="1"
else
	conf_fraction="0.${conf_percent}"
fi
alingmenttype=""
if [[ ${AB} == "true" ]]; then
  alingmenttype="--AB"
fi
gene_pos_inclusiveness=""
if [[ ${gene_start_pos_inclusive} == "false" ]]; then
	gene_pos_inclusiveness="$gene_pos_inclusiveness --gene_start_exclusive"
fi
if [[ ${gene_stop_pos_inclusive} == "true" ]]; then
	gene_pos_inclusiveness="$gene_pos_inclusiveness --gene_stop_inclusive"
fi
cmd="${sparksubmit} ${jar} locateMotifs --motifs ${motifsWithConf} --fasta ${fasta} --input ${inputfolder} --output ${motifsPositions} --bindir ${bin} --partitions ${PARTITIONS} --degen ${degen} --max_len $(echo $length | awk -F"," '{print $2}') --bls_thresholds ${blsthresholds} ${alingmenttype} --conf_cutoff ${conf_fraction} --fam_cutoff ${ortho_count_cutoff} ${gene_pos} ${gene_pos_inclusiveness}"
echo ${cmd}
${cmd}

if [[ -f ${motifsPositions}/_SUCCESS ]]; then
	# N=$(cat ${motifsPositions}/* | wc -l)
	# echo "found ${N} locations"
  echo "output located here: ${motifsPositions}"
else
  echo "getting motif locations failed, exiting"
  exit 2;
fi
