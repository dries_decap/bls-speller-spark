# Filtering redundant motifs (output of BLSSpeller)
# This requires Julia 1.6.1 or higher

# This julai script filter redundant motifs based on:
    # - motif similarity (pairwise blast)
    # - overlap in genes overlap (it can be based on instances overlap)

# import packegaes: install packages if not already installed
using Pkg

# function for check whether a package installed already
isinstalled(pkg::String) = any(x -> x.name == pkg && x.is_direct_dep, values(Pkg.dependencies()))

isinstalled("BioAlignments")==false ? Pkg.add("BioAlignments") : nothing
isinstalled("ArgParse")==false ? Pkg.add("ArgParse") : nothing
isinstalled("CSV")==false ? Pkg.add("CSV") : nothing
isinstalled("DataFrames")==false ? Pkg.add("DataFrames") : nothing
isinstalled("Statistics")==false ? Pkg.add("Statistics") : nothing
isinstalled("Random")==false ? Pkg.add("Random") : nothing
isinstalled("StatsBase")==false ? Pkg.add("StatsBase") : nothing



using BioAlignments
using ArgParse
using CSV
using DataFrames
using Statistics
using Random
using StatsBase

println("Required packages have been installed and loaded")

# function to parse arguments
function parse_arguments()
    PA = ArgParseSettings()
    @add_arg_table PA begin
        "motif_PATH"
            help = "path to the motif output (bed files) of the species of interest (e.g. Zea mays)"
            required = true
        "degenerate"
            help = "number of degenerate sites allowed in a motif"
            arg_type = Int
            default = 3
        "motif_l"
            help = "length of a motif"
            arg_type = Int
            default = 8
        "gene_overlap_p"
            help = "proportion of allowed overlap between gene sets of two motifs"
            arg_type = Float64
            default = 0.5
        "big_off"
            help = "percentage of motifs to be removed as corresponding to general TFs"
            arg_type = Float64
            default = 0.01
        "small_off"
            help = "lower bound cutoff for gene set size"
            arg_type = Int
            default = 20
        "out_path"
            help = "file name to write output"
            arg_type = String
            default = "non_redundant_motifs.txt"
    end
    return parse_args(PA)
end


# arguments
AP = parse_arguments()

# function to save the motifs and associated genes in a dict
function load_motifs(motif_PATH=AP["motif_PATH"])
    # get the motifs name
    motif_files=readdir(motif_PATH);
    motifs_dt = Dict();
    for i in motif_files
        motif = replace(i, ".bed"=>"")
        motif == "" && continue
        !(occursin(".bed", i)) && continue
        genes = CSV.File(motif_PATH *i, header=false, delim = "\t") |>
            DataFrame |> x -> x[:,5] |> unique
        motifs_dt[motif] = genes
    end
    return motifs_dt
end

#function to calculate nucleotide composition
function nucl_compos(motif_dt)
    motif_comp = Dict()
    for motif in keys(motif_dt)
       for c in motif
            if c ∉ keys(motif_comp)
                motif_comp[c] = 1
            else
                motif_comp[c] += 1
            end
        end
    end
    return motif_comp
end

# function to return the number of degenerative bases in a motif
function degenerate_l(motif)
    motif = uppercase(motif)
    non_degenerate = ['A', 'T', 'C', 'G']
    return sum([i ∉ non_degenerate for i in motif])
end

# function to generate random motifs with the same nucleotide composition as predicted motifs
function generate_random_motif(l, motif_cmp_dt)
    chars = [i for i in keys(motif_cmp_dt)]
    weight = [i for i in values(motif_cmp_dt)]
    sample_chars = sample(chars, Weights(weight),  l, replace=true)
    return join(sample_chars)
end

# function to generate random motifs and keep those with at most 3 degenerate bases (as the predicted motifs)
function generate_random_motifs(motif_l=AP["motif_l"], motif_cmp_dt=motif_cmp_dt, motifs_dt=motifs_dt, degenerate= AP["degenerate"])
    Random.seed!(0)
    random_motifs = [generate_random_motif(motif_l, motif_cmp_dt) for _ in 1:length(motifs_dt) + 1000];
    random_motifs = [i for i in random_motifs if degenerate_l(i) <= degenerate]
    random_motifs  = sample(random_motifs, length(motifs_dt), replace=false);
    return random_motifs
end

# function to calculate the pairwise distance between random motifs to estimate the distance threshold to filter redundancy in predicted motifs
function find_dist_thresh(random_motifs, do_plot = false)
    costmodel = CostModel(match=0, mismatch=1, insertion=1, deletion=1);
    all_dist = []
    all_mot = Set([i for i in random_motifs]);

    while length(all_mot) > 0
        i = pop!(all_mot)
        for j in all_mot
            vv = pairalign(EditDistance(), i, j, costmodel) |> distance
            #println(pairalign(GlobalAlignment(), i, j, scoremodel))
            push!(all_dist, vv)
        end
    end
    if do_plot == true
        histogram(all_dist)
        vline!([quantile(all_dist, 0.05)], legend = false, ylab = "Frequnecy", xlab = "Distnace" ) # 0.95 quantile of the random motifs
    end
    return quantile(all_dist, 0.05)
end

#Filtering redundant motifs
# Remove motifs with less degenerate sites, if degenerate sites are equal, remove the motif with smaller gene set
#Parameters:
    #-motifs_dt: a dict object with motifs as keys and associated genes as values
    #-big_off: remove motifs with a large geneset (default 1% of motifs); assumption: those motifs most likely correspond to general TFs
    #-small_off: remove motifs with a small geneset size (default 20 gens): it's hard to validate motifs with only few nearby genes
    #-motif_l: length of motifs (default = 8)
    #-gene_overlap_p: threshold for proportion of **gene set overlap** (default = 0.5)
    #- threshold distance between motifs (to be estimated from random motif, 0.95 quantile, see above)
    #-out_path: file name to write the name of non-redundant motifs: default=non_redundant_motifs.txt

function filtering_fun(motif_PATH=AP["motif_PATH"], big_off = AP["big_off"], small_off = AP["small_off"], gene_overlap_p = AP["gene_overlap_p"],
                       motif_l = AP["motif_l"], out_path=AP["out_path"])
    # load motifs
    motifs_dt = load_motifs(motif_PATH)
    println("motifs have been loaded")
    println(length(motifs_dt))
    # number of genes with at least one instance for each motif
    motifs_instances_n = [length(j) for j in values(motifs_dt)];
    println("pass5")
    # remove 1% of motifs (correposnd to general TFs)
    motifs_dt = Dict(i=>j for (i,j) in motifs_dt if length(j) < quantile(motifs_instances_n, 1 - big_off))
    println("%$big_off of motifs have have been removed as corresponding to general TFs")
    # remove motifs with few number of genes
    motifs_dt = Dict(i=>j for (i,j) in motifs_dt if length(j) > small_off);

    # geneset size for each motif
    motif_len = Dict(i=>length(motifs_dt[i]) for i in keys(motifs_dt));

    # Calculate the nucleotide composition
    motif_cmp_dt = nucl_compos(motifs_dt)
    # generate 1000 random motifs
    random_dt = generate_random_motifs(motif_l, motif_cmp_dt,motifs_dt)
    # calculate the pairwise distanse between random motifs (null distribution of distance between motifs)
    dist_threshold = find_dist_thresh(random_dt)
    println("Only 5% of random motifs show distnace smaller than $(dist_threshold +1)")

    costmodel = CostModel(match=0, mismatch=1, insertion=1, deletion=1);
    all_mot = Set(i for i in keys(motifs_dt)); # collect all the motifs keys
    del_motifs = [] # collect the motifs to be removed from the list

    # pick one motif at a time and compare it all remaining motifs in the list
    println("start filtering motifs ...")
    while length(all_mot) > 0
        i = pop!(all_mot)
        for j in all_mot
            # number of shared genes by motif i and j
            shared_genes = intersect(motifs_dt[i], motifs_dt[j]) |> length

            # number of degenerate sites for each motifs
            num_degen_i = degenerate_l(i)
            num_degen_j = degenerate_l(j)

            # distnace between motif i and j based on pairwise alignment
            dist = pairalign(EditDistance(), i, j, costmodel) |> distance

            # find the motif with smaller gene set
            small_m = motif_len[i] < motif_len[j] ? i : j
            big_m = motif_len[i] <= motif_len[j] ? j : i

            # proportion of shared genes with respect to motifs with smaller gene set
            shared_tresh = shared_genes / min(motif_len[small_m], motif_len[big_m])
            println("all_good")
            # filter motif i if it is similar to motif j and share the same gene set
            if (dist <= dist_threshold  && shared_tresh > gene_overlap_p)
                println("all_ggod2")
                # delete motif with less degenerate bases
                if num_degen_i != num_degen_j
                    num_degen_i > num_degen_j ? push!(del_motifs, j) : push!(del_motifs, i)
                    num_degen_i < num_degen_j && break
                    num_degen_j < num_degen_i && delete!(all_mot, j)
                end
                println("all_ggod3")

                # if degenerates site are equal, delete motif with smaller gene set
                if num_degen_i == num_degen_j
                    push!(del_motifs, small_m)
                    small_m == i ? break : delete!(all_mot, j)
                end
                println("all_ggod4")

            end
        end
    end

    non_redun_motifs = setdiff(keys(motifs_dt), del_motifs)
    println(length(del_motifs), " motifs have been removed")
    println("The totlal number of non-redundant motifs are: ", length(non_redun_motifs))

    # write non-redundant motifs names into a file
    open(out_path, "w") do handle
        for key in non_redun_motifs
            write(handle, key*"\n")
        end
        close(handle)
    end
    println("The list of non-redundant motifs have been writen to $out_path")
end

# call the filtering function
filtering_fun()


