#!/bin/bash

# BLS SPELLER VARIABLES
length=8,9 # range, 8,10 -> [8,10[ gives lengths 8 and 9
degen=3
alphabet=3 # 3 - full iupac, 2 is twofoldAndN, 1 is exact+N, 0 is only exact
blsthresholds="0.07,0.13,0.41,0.54,0.65,0.75,0.85,0.95"
AB=false

# FOR MOTIF POSITIONS
conf_percent=90 # confidence score cutoff (bls cutoff can be done by filtering the output on bls scores)
ortho_count_cutoff=1 # is useful to increase if counts are very low, which can lead to a confidence score of 100% when its not the case (typically only needed with lengths of 10 or more)
use_gene_pos=false # set to true if you want gene position rather than absolute genomic position
gene_start_pos_inclusive=true # is start pos of promotor region in fasta file included, i.e. if pos is 100, is the first character at pos 100 (inclusive) or 101 (exclusive)?
gene_stop_pos_inclusive=false # is stop pos of promotor region in fasta file included, i.e. if pos is 100, is the last character at pos 99 (exclusive) or 100 (inclusive)?

# INPUT OUTPUT, all I/O dirs should be accessible from all nodes in the cluster
fasta=$HOME/blsfastas/zma.fasta # maize fasta
inputfolder=$HOME/blsinput # folder with all cluster files
outputfolder=$HOME/blsoutput # in here folders will be created per step:
# this can remain the same if you change outputfolder per run or have removed outputfolder before the run
motifsWithConf=$outputfolder/l${length/,/-}_a${alphabet}_d${degen}_motifs
motifsPositions=$outputfolder/l${length/,/-}_a${alphabet}_d${degen}_c${conf_percent}_positions

# LIB/BIN DIRS
BLS_VERSION=1.1
bin=$HOME/bin
lib=$HOME/lib

# SPARK VARIABLES, should be like this for skitty, big_execmem might need to be more with lower confidence scores
STAGING_DIR=$HOME/hod/sparkStaging # should point to a network storage with lots of capacity, intermediate files will be stored here
mkdir -p $STAGING_DIR
CORES=20
NODES=2
JOB1_CPUS=4
if [[ $((CORES % JOB1_CPUS)) == 0 ]]; then JOB1_EXECUTORS=$((NODES * CORES / JOB1_CPUS - 1)); else JOB1_EXECUTORS=$((NODES * CORES / JOB1_CPUS)); fi;
JOB1_EXECMEM="3500m" # memory available per cpu core ( = total_memory / nr_cpu )
JOB2_CPUS=1
if [[ $((CORES % JOB2_CPUS)) == 0 ]]; then JOB2_EXECUTORS=$((NODES * CORES / JOB2_CPUS - 1)); else JOB2_EXECUTORS=$((NODES * CORES / JOB2_CPUS)); fi;
JOB2_EXECMEM="4000m" # memory available per cpu core ( = total_memory / nr_cpu )
JOB2_OVERHEAD_MEM=2000
