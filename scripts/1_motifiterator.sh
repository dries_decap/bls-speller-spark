#!/bin/bash

. bls_config.sh


# THIS SHOULD REMAIN THE SAME
v=$BLS_VERSION
jar=${lib}/bls-speller-assembly-${v}.jar
jars=${jar}
class=be.ugent.intec.ddecap.BlsSpeller
memory="--executor-memory ${JOB1_EXECMEM} --num-executors ${JOB1_EXECUTORS} --executor-cores ${JOB1_CPUS} --conf spark.task.cpus=${JOB1_CPUS}" # --conf spark.executor.memoryOverhead=3000"
sparksubmit="spark-submit --master yarn --conf spark.ui.showConsoleProgress=true ${memory} --conf spark.yarn.stagingDir=$STAGING_DIR --jars ${jars} --class ${class}"
PARTITIONS=${JOB1_EXECUTORS}

alingmenttype=""
if [[ ${AB} == "true" ]]; then
  alingmenttype="--AB"
fi

cmd="${sparksubmit} ${jar} getMotifs --input ${inputfolder} --output ${motifsWithConf} --bindir ${bin} --partitions ${PARTITIONS} --alphabet $alphabet --degen ${degen} --min_len $(echo $length | awk -F"," '{print $1}') --max_len $(echo $length | awk -F"," '{print $2}') --bls_thresholds ${blsthresholds} ${alingmenttype}"
echo ${cmd}
${cmd}

if [[ -f ${motifsWithConf}/_SUCCESS ]]; then
  # remove the unmerged files if successful
	N=$(cat ${motifsWithConf}/* | wc -l)
	echo "found ${N} motifs"
  echo "output located here: ${motifsWithConf}"
else
  echo "getting motifs failed, exiting"
  exit 2;
fi
