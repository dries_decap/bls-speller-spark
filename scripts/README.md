# required tools:
- [blsspeller jar](https://bitbucket.org/dries_decap/bls-speller-spark/downloads/bls-speller-assembly-0.2.jar)
- [motifIterator binary built](https://bitbucket.org/dries_decap/suffixtree-motif-speller/src/master/motifIterator/)

# STEP 1: Collecting all motifs with a confidence score of at least 50%

## configuration file

First adjust all variables in ``bls_config.sh`` to your needs.

> **_NOTE:_**  This step uses a c++ program which requires the Apache Arrow library and runs through Apache Spark (with YARN).

```bash
# ADJUST THE VARIABLES IN THE bls_config.sh SCRIPT!
./1_motifiterator.sh
```

# STEP 2: Filter motifs on thresholds and find corresponding location in a species
> **_NOTE:_**  This runs on Apache Spark (with YARN).

Here we filter motifs on certain a Confidence threshold and we locate these in the requested species.
```bash
# ADJUST THE VARIABLES IN THE bls_config.sh SCRIPT!
./2_map_motifs_to_fasta.sh
```

# STEP 3: Filtering redundant motifs 
> **_NOTE:_** Requirs Julia version 1.6 or newer.

```bash
# the input is a folder containing the bed files of a single species.
julia  filtering_script.jl "/path/to/bls_output/motifs_zma/"
```
