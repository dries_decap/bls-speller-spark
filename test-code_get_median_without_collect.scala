import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}
import scala.collection.mutable.ListBuffer
import be.ugent.intec.ddecap.dna.LongEncodedDna._
import be.ugent.intec.ddecap.dna.{BlsVector, ImmutableDnaWithBlsVector}
import org.apache.spark.storage.StorageLevel
import be.ugent.intec.ddecap.Timer
import be.ugent.intec.ddecap.spark.DnaStringPartitioner
import be.ugent.intec.ddecap.dna.BinaryDnaStringFunctions._
import collection.mutable.PriorityQueue;
import scala.collection.mutable.ArrayBuffer
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types._
import be.ugent.intec.ddecap.spark.BlsParquet._
import be.ugent.intec.ddecap.tools.FileUtils.deleteRecursively

val byteToAscii = Array(' ', 'A', 'C', 'M', 'G', 'R', 'S', 'V', 'T', 'W', 'Y', 'H', 'K', 'D', 'B', 'N')
type ImmutableDna = Long
def fact(n: Int) : Int = {
  var f = 1
  for (i <- 2 to n) {
    f = f * i
  }
  return f
}
def getNumberOfPermutationsFromString(group: String): Int = {
  val charcounts = group.groupBy(identity).map(x => (x._1, x._2.size))
  var tmp = 1
  for (c <- charcounts) {
    tmp = tmp * fact(c._2)
  }
  return fact(group.size)/tmp
}

val path = "/hadoop/bls-speller-spark/data/output_test4/output.parquet/"
val outputmotifs = spark.read.parquet(path)

val familyCountCutOff = 20
val confidenceScoreCutOff = 0.95f
val thresholds = (outputmotifs.schema.length - 1) / 2
val filtered = outputmotifs.flatMap(x => {
  var idx = 0
  while(idx < thresholds && !(x.getInt(1+idx) > familyCountCutOff && x.getFloat(1+thresholds+idx) > confidenceScoreCutOff))
    idx += 1
  if(idx < thresholds) Some(x.getString(0) + "\t" + idx) else None
}).toDF("motif")

// val path = "/hadoop/bls-speller-spark/data/output_test3/l8-10_d3_merged/"


val path = "/hadoop/bls-speller-spark/data/output_test4/l8-12_d3_merged/"
val motifs = spark.read.parquet(path)
val thresholds = 7

val motifsdf=readRawParquet(spark, thresholds, "/hadoop/bls-speller-spark/data/output_test3/l8-10_d3_merged/")
deleteRecursively("/hadoop/bls-speller-spark/data/output_test3/l8-10_d3_merged-test/")
motifsdf.write.parquet("/hadoop/bls-speller-spark/data/output_test3/l8-10_d3_merged-test/")
info(Timer.measureTotalTime("BlsSpeller - merge Parquet Motifs"))



sc.setCheckpointDir("/tmp/checkpoints/")

val medianKs = motifs.select("group").groupBy("group").count().map(x => {
   val perms = getNumberOfPermutationsFromString(x.getString(0))
   val nr = (perms + 1) / 2
   (x.getString(0), (if (x.getLong(1) > nr) (x.getLong(1) - nr - 1)  else -1))
 }).toDF("group", "row_number").checkpoint()
medianKs.count

val medians = ArrayBuffer.empty[DataFrame]
for (blsThreshold <- 0 until thresholds) {
  @transient val blsThresholdOrder  = Window.partitionBy("group").orderBy("bls" + blsThreshold)
  val ordered = motifs.select("group", "bls" + blsThreshold).withColumn("row_number", row_number().over(blsThresholdOrder) )
  medians += ordered.join(broadcast(medianKs), Seq("group", "row_number"), "left").select(col("group"), col("bls" + blsThreshold).as("median" + blsThreshold)).checkpoint()
}
val allmedians = medians.reduce(_.join(_, Seq("group"), "outer")).checkpoint()
allmedians.count

val conf = motifs.filter(Range(0, thresholds).map(x => motifs("bls" + x) > familyCountCutOff).reduce(_||_)).join(broadcast(allmedians), Seq("group"), "left").flatMap(row => {
  val conf_scores = ArrayBuffer.empty[Float]
  val counts = ArrayBuffer.empty[Int]
  var passes = false;
  for (i <- 0 until thresholds) {
    val observed_count  = row.getInt(2 + i);
    val median_count : Float = if(row.isNullAt(thresholds + 2 + i)) 0 else row.getInt(thresholds + 2 + i);
    counts += observed_count
    conf_scores += (if (observed_count > median_count && observed_count > familyCountCutOff) (observed_count - median_count) / observed_count else 0.0f);
    if(conf_scores.last > confidenceScoreCutOff) passes = true;
  }
  if(passes) Some(row.getString(1), counts, conf_scores) else None
}).select(
  Seq(col("_1").as("motif")) ++
  Range(0, thresholds).map(i => col("_2")(i).as("bls" + i)) ++
  Range(0, thresholds).map(i => col("_3")(i).as("conf" + i)) : _*
)

conf.write.parquet("/tmp/test_threshold_" + blsThreshold + ".parquet")


conf.sort("motif").write.parquet("/tmp/test_threshold_" + blsThreshold + "-sorted.parquet")







//------------------------------------
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.hadoop.conf.Configuration
import java.net.URI
val conf = new Configuration(sc.hadoopConfiguration)
val outputFolder= "/tmp/parquet/"
val tmpPath = new Path(outputFolder + "/output")
val outPath = new Path(outputFolder)
val fs = FileSystem.get(new URI(outputFolder), conf);

fs.rename(tmpPath, outPath)

// -----------------------------------
val thresholds = 5
val path = "/tmp/parquet.out"
val test1 = spark.read.option("mergeSchema", "true").schema(createSchema(thresholds)).parquet(path)
test1.printSchema
val path = "/tmp/parquet_int32.out"
val test2 = spark.read.parquet(path)
test2.printSchema
test2.show
def countPerMotif(input: DataFrame) : DataFrame = {
  input.groupBy("group", "motif").agg(sum("bls0").cast(org.apache.spark.sql.types.IntegerType).as("bls0"), Range(1, input.schema.length - 2).map(x => sum("bls" + x).cast(org.apache.spark.sql.types.IntegerType).as("bls" + x)) :_*)
}
val tmp = countPerMotif(test2)
tmp.printSchema
