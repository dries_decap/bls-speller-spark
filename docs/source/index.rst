.. blsspeller documentation master file, created by
   sphinx-quickstart on Mon May  2 10:02:50 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

BLS Speller Spark Documentation
===============================

The BLS Speller Spark software is used to detect cis-regulatory motifs using orthologous gene information from different species.
An additional step is added compared to the original tool, which can map back conserved motifs to their locations in the gene promotors or specific species.

.. note:: BLS Speller Spark is available under the GNU license and uses opensource tools which need to be added to every node in the cluster in a specified directory.



Contents:

.. toctree::
  :maxdepth: 2

  contents/blsspeller
  contents/prep
  contents/motifiterator
