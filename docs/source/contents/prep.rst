Preprocessing
=============

Installation
------------

Several files need to be preprocessed in order to use the BLS speller. The orthologous families can be created with the ``prepInput`` tool like this:

.. code-block:: bash
	:linenos:

	# clone git repo if not done yet
	git clone https://dries_decap@bitbucket.org/dries_decap/suffixtree-motif-speller.git
	cd suffixtree-motif-speller/process_input
	mkdir build
	cd build
	cmake ../
	make

Execution
---------

This tool requires three inputs:

- an orthology file: a tab separated file with this format per line ``#ID\tNumGenes\tNumSpecies\tspace separated gene list``
- a folder with a subfolder per species (by name) with a fasta file in that species folder where there is a promotor sequence available for all the genes present in the orthology file.
- a newick tree that represents the present species

And can be run like this:

.. code-block:: bash
	:linenos:

	./prepInput ${fasta_files_folder} ${orthology_file} ${newick_tree_file} ${outputfolder}
