Motif iterator
==============

Installation
--------------

.. code-block:: bash
	:linenos:

	# install apache arrow libraries; based on https://arrow.apache.org/install/
	sudo apt-get update
	sudo apt-get install -y -V ca-certificates lsb-release wget
	wget https://apache.jfrog.io/artifactory/arrow/$(lsb_release --id --short | tr 'A-Z' 'a-z')/apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
	sudo apt-get install -y -V ./apache-arrow-apt-source-latest-$(lsb_release --codename --short).deb
	sudo apt-get update
	sudo apt-get install -y -V libarrow-dev libarrow-glib-dev libparquet-dev libparquet-glib-dev

	# clone git repo if not done yet
	git clone https://dries_decap@bitbucket.org/dries_decap/suffixtree-motif-speller.git
	cd suffixtree-motif-speller/motifIterator
	mkdir build
	cd build
	cmake ../
	make

Execution
----------
Run the motif iterator like this:

.. code-block:: bash
	:linenos:

	input="/data/bls/wheat/wheat_12/GENE_FAMILY_0001"
	output="/tmp/gene_family_0001.parquet"
	bls_thresholds="0.1,0.5,0.6,0.7,0.8" # comma separated list of bls thresholds
	length=8
	degen=3
	./motifIterator discovery ${input} --bls ${bls_thresholds} --length ${length} --fullIupac --degen ${degen} --parquet --output ${output}

	# or in counted mode if multiple gene families are in a single file
	./motifIterator discovery ${input} --bls ${bls_thresholds} --length ${length} --fullIupac --degen ${degen} --parquet --output ${output} --count

	# or read data from stdin to process multiple files
	input="/data/bls/wheat/wheat_12/GENE_FAMILY_000*"
	cat ${input} | ./motifIterator discovery - --bls ${bls_thresholds} --length ${length} --fullIupac --degen ${degen} --parquet --output ${output} --count
